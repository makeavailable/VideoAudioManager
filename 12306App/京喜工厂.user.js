// ==UserScript==
// @name         京喜工厂
// @namespace    http://tampermonkey.net/
// @version      0.5.6
// @description  try to take over the world!
// @author       makelage
// @match        *://st.jingxi.com/pingou/dream_factory/*
// @match        *://st.jingxi.com/pingou/factoryv2/*
// @match        *://*.jingxi.com/*
// @match        *://*.jd.com/*
// @grant        none
// @requireaaa      http://libs.baidu.com/jquery/2.0.0/jquery.min.js
// ==/UserScript==

(function() {
    'use strict';
    console.log(window.location.href);;

    class ElectFactory{
        constructor(){
            this.last_v = "";
            this.loop = 0;
            this.next = 0;
        }
        work(){
            this.get_电力();
        }
        get_电力(){
            this.loop += 1;
            if (this.loop > 20){
                window.location.href = window.location.href;
                return;
            }
            console.log(this.last_v + "|" + $("div.alternator-num-n").innerHTML);
            if ($("div.simple_dialog_btn") && $("div.simple_dialog_btn").innerHTML.indexOf("邀请好友打工") >= 0){
                $("div.close").click();
                return;
            };
            if ($("div.g_error_btn")){
                if ($("div#dialog")){
                    $("div.close").click();
                }
                $("div.g_error_btn").click();
                return;
            }
            if ($("div.select_goods")) {
                $("div.close").click();
                return;
            }
            if ($("div.dialog_btn.short")) {
                $("div.close").click();
                return;
            }
            if ($("div.m_power_bolts")) {
                for (let index = 0; index < $("div.m_power_bolts").children.length; index++) {
                    const element = $("div.m_power_bolts").children[index];
                    if (element.getAttribute("class").indexOf("collect") >= 0) {
                        continue;
                    }
                    let e = element.getElementsByClassName("m_power_bolt_tag");
                    if (e.length > 0 && e[0].innerText === "招工提升电力") {
                        continue;
                    }
                    e = element.getElementsByTagName("span");
                    if (e.length > 0) {
                        e[0].click();
                        return;
                    }
                }
            }
			if ($("div.m_worker_rec_award")) {
				$("div.m_worker_rec_award").click();
				return;
			}
            if (this.loop < 3){
                this.last_v = $("div.alternator-num-n").innerHTML;
                return;
            }
            if ($("div.alternator-num-n").innerHTML !== this.last_v || $("div.m_progress_btn")) { //$("div.m_progress_btn") 没有产品
                if ((this.last_v !== "0" || $("div.m_progress_btn")) && this.next){
                    this.next.work();
                }
                if (!$("div.close") && $("div.m_progress_btn") && $("div.m_progress_btn").innerText.indexOf("投入商品") >= 0) {
                    $("div.icon.icon_task").click();
                    return;
                }
                return;
            }
            this.last_v = $("div.alternator-num-n").innerHTML;
            if (this.last_v.indexOf(".") >= 0) {
                return;
            }
			if ($("span.top-l-info-n").innerHTML > 100 && $("div.icon_add_num") && $("div.icon_add_num").innerHTML.indexOf('0 /') > -1){
                $("div.icon_add_num").click();
                return;
			}
            //发电机满
            if (window.location.href.indexOf("?") >= 0){
                window.location.href = window.location.href.substring(0,window.location.href.indexOf("?"));
            	return;
            }
            if ($("div.alternator-bg")){  //发电机
                $("div.alternator-bg").click();
            }
            if ($("div.simple_dialog_btn")){ //作任务
                $("div.simple_dialog_btn").click();
                return;
            }
        }
    }
    class TaskManager{
        constructor(){
            this.has_task = 0;//0未开，1有任务，2当前tab完成，3全部完成
            this.tab_ndx = 0;
            this.next = 0;
            this.has_part = 1;//0无 1有 2上限
            this.compent_count = 0;
        }
        work(){
            if (this.has_task == 3 && this.has_part == 0){
                if (window.location.href.indexOf("?") >= 0){
                    window.location.href = window.location.href.substring(0,window.location.href.indexOf("?"));
                    return;
                } else{
                    window.location.href = window.location.href;
                    return;
                }
            }
            this.click_redpoint();
            this.pick_part();
        }
        pick_part(){//捡部件
            if ($("div.g_error_title.line1") && $("div.g_error_title.line1").innerHTML.indexOf('今日已达领取上限了哦')>=0){
                this.has_part = 2;
                $("div.close").click();
                return;
            }
            if (this.has_part == 2){
                return;
            }
            var e = document.getElementsByClassName("m_part anim");
            if (this.compent_count == e.length){
                console.log("今日已达领取上限了哦");
                return;
            }
            this.compent_count = e.length;
            if (e.length > 0){
                e[0].click();
                if (e.length > 1){
                    e[1].click();
                }
                this.has_part = 1;
            } else {
                this.has_part = 0;
            }
        }
        click_redpoint(){
            var e = document.getElementsByClassName("task_tabs_tab");//任务界面的3个tab
            if (e.length == 0){//任务界面没打开
                if ($("div.icon_task.red_dot")){
                    this.has_task = 2;
                    $("div.icon_task.red_dot").click();
                    return;
                }
                this.has_task = 3;
                //$("div.icon_task").click();
                return;
            }
            if (this.has_task == 0 || this.has_task == 2){
                for (; this.tab_ndx < e.length-1; ++this.tab_ndx){
                    var btn = e[this.tab_ndx];
                    if (btn.getAttribute("class").indexOf("red_dot") < 0){
                        continue;
                    }
                    this.has_task = 1;
                    btn.click();
                    return;
                    //完成任务
                }
            }
            e = document.getElementsByClassName("task_item_btn btn_type_2");//可以领取的
            if (e.length == 0){
                if (this.tab_ndx == 2){//两页都没有红点了
                    this.do_task();
                } else {
                    this.has_task = 2;
                }
                return;
            }
            this.has_task = 1;
            e[0].click();

        }
        do_task(){
            var task_list = document.getElementsByClassName("task_item");
            var i = 0;
            for (i=0;i<task_list.length;++i){
                var e = task_list[i];
                var type = e.getElementsByClassName("task_item_btn btn_type_1");
                if (type.length == 0){
                    continue;
                }
                var info = e.getElementsByClassName("task_item_info_name")[0].innerText;
                if (info.indexOf("给工厂招募工人") >= 0 ){
                    continue;
                }
                if (info.indexOf("登录APP领取奖励") >= 0 ){
                    continue;
                }
                if (info.indexOf("招工") >= 0 ){
                    continue;
                }
                console.log(info);
                type[0].click();
                return;
            }
            this.has_task = 3;
            $("div.close").click();//关闭界面
        }
    }

    var obj ;
    function index_page(){
        if (!obj){
            obj = new ElectFactory();
            obj.next = new TaskManager();
        }
        obj.work();
    }

    function do_market_task(){
        console.log("do_market_task:"+document.URL);
        var taskid = document.URL.split("taskId=")[1];
        if (!taskid){
            return scrollPage();
        }
        taskid = taskid.split("&")[0];
        if (taskid == "19"){
            return find_box();
        } else if (taskid == "15"){
            return scrollPage();
        }else if (taskid == "16" || taskid === '17'){
            return click_5_item();
        }else if (taskid == "21" || taskid == "20"){
            return find_box();
        }else if (taskid == "140"){
            return stop_10_s();
        }else if (taskid == "58"){
            return stop_15_s();
        }
        return scrollPage();
    }

    function find_box(){
        var div=document.getElementsByClassName("boxs");
        if (div.length == 0){
            return go_back();
        }
        var lst = div[0].childNodes;
        if (lst.length == 0){
            return go_back();
        }
        div=document.getElementsByClassName("close");
        var i = 0;
        for (i=0; i < div.length;++i){
            div[i].click();
        }
        for (i=0; i < lst.length;++i){
            div = lst[i];
            if (!div.tagName){
                continue;
            }
            div.click();
            return;
        }
        return go_back();
    }
    function click_5_item(){

        var e=document.getElementsByClassName("floating_finish");
        if (e.length > 0) {
            window.localStorage.setItem("itemIndex", 0);
            e[0].click();
            return;
        }
        e=document.getElementsByClassName("floating_prize");
        if (e.length > 0) {
            e[0].click();
            window.localStorage.setItem("itemIndex", 0);
            return;
        }
        let i = 0;
        e=document.getElementsByClassName("floating_title");
        if (e.length > 0) {
            i = e[0].innerText.split('/')[0]
        } else {
            i = window.localStorage.getItem("itemIndex");
        }
        if (!i){
            i = 0;
        } else {
            i = parseInt(i);
        }
        var lst = document.getElementsByClassName("tuan_recommend_content_item");
        if (lst.length <= i){
            scrollPage()
            console.log("浏览5件商品--没有商品");
            return;
        }
        e = lst[i];
        window.localStorage.setItem("itemIndex", i+1);
        e.getElementsByClassName("tuan_recommend_content_item_img")[0].click()
    }

    function stop_10_s(){
        if (!obj){
            obj = 1;
            return;
        }
        ++obj;
        if (obj == 3){
            return go_back();
        }
    }
    function stop_15_s(){
        if (!obj){
            obj = 1;
            return;
        }
        ++obj;
        if (obj == 5){
            return go_back();
        }
    }


    function scrollPage(){
        //$("div.scroll-view.market-scroll.scroll-y").scrollTop = 100;
        //$("div.floating_finish").click();
        //$("div.floating_prize").click();
        console.log(window.$);
        var e = document.getElementsByClassName("scroll-view market-scroll scroll-y");
        e[0].scrollTop=100;
        e[0].scrollTop=200;
        e=document.getElementsByClassName("floating_finish");
        if (e.length > 0) {
            e[0].click();
        }
        e=document.getElementsByClassName("floating_prize");
        if (e.length > 0) {
            e[0].click();
        }
    }
    function go_back(){
        window.history.back();
    }
    function jxnc(){
        go_back();
    }
    if (document.URL.indexOf( "st.jingxi.com/pingou/dream_factory/market.html") >= 0) {
        setInterval(do_market_task, 3000);
    }
    else if (document.URL.indexOf( "st.jingxi.com/pingou/factoryv2/index.shtml") >= 0) {
        setInterval(do_market_task, 3000);
    }
    else if (document.URL.indexOf( "dream_factory/divide.html") >= 0) {
        setInterval(do_market_task, 3000);
    }
    else if (document.URL.indexOf( "m.jingxi.com/webportal/event/30200") >= 0) {
        setInterval(go_back, 12000);
    }
    else if (document.URL.indexOf( "m.jingxi.com/item/") >= 0) {
        setInterval(go_back, 3000);
    }
    else if (document.URL.indexOf("st.jingxi.com/pingou/dream_factory/index") > 0)
    {
        setInterval(index_page, 3000);
    }
    else if (document.URL.indexOf( "actst.jingxi.com/pingou/taskcenter/index.html") >= 0) {
        setInterval(go_back, 12000);
    }
    else if (document.URL.indexOf( "st.jingxi.com/sns/") >= 0) {
        setInterval(jxnc, 12000);
    }
    else if (document.URL.indexOf(".jd.com")>=0) {
        if (document.URL.indexOf("bizCode=dream_factory")>=0 && document.URL.indexOf("taskId=")>=0) {
            setInterval(go_back, 12000);
        }
        // else {
        //     setInterval(go_back, 12000);
        // }
    } else {
        setInterval(go_back, 12000);
    }
})();