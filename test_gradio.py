import requests
import json
import gradio as gr
from tenacity import retry, stop_after_attempt

@retry(stop=stop_after_attempt(3))
def stream_ai_response(prompt):
    path = r'D:\code\VideoAudioManager\newRoute-yase.png'
    authorization = 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyLWNlbnRlciIsImV4cCI6MTc0NTQ5NzI4MywiaWF0IjoxNzM3NzIxMjgzLCJqdGkiOiJjdTlvYmdxaHJhMGJ0dmhwa3Q3MCIsInR5cCI6ImFjY2VzcyIsImFwcF9pZCI6ImtpbWkiLCJzdWIiOiJjdTMxdm90M3Y4OXZrbWQzdWRuMCIsInNwYWNlX2lkIjoiY3UzMXZvdDN2ODl2a21kM3VkbWciLCJhYnN0cmFjdF91c2VyX2lkIjoiY3UzMXZvdDN2ODl2a21kM3VkbTAiLCJyb2xlcyI6WyJ2aWRlb19nZW5fYWNjZXNzIl0sInNzaWQiOiIxNzMwNDgxNzY4NTQyOTQ1NTIxIiwiZGV2aWNlX2lkIjoiNzQwMjk4OTkyNDEzNzUxMTQyNSJ9.Hp0cAgnMLdzL6EcfVLG6AmfhszBewLLoKjtLIidHFRdDAxvFes50o0BMXXebWW8HCgxTube7bVOyEXRIeQh4EQ'
    cookie='lang=zh-CN;'
    headers = {
        "authorization": authorization,
        "content-type": 'application/json',
        'referer': 'https://kimi.moonshot.cn/',
        'cookie': cookie
    }

    url = 'https://kimi.moonshot.cn/api/chat/cute3e697i0mbldknevg/completion/stream'
    data = {
        "kimiplus_id": "kimi",
        "extend": {
            "sidebar": True
        },
        "model": "kimi",
        "use_research": False,
        "use_search": True,
        "messages": [
            {
                "role": "user",
                "content": prompt
            }
        ],
        "refs": [
        ],
        "history": [],
        "scene_labels": []
    }

    stream = True

    try:
        with requests.post(url, json=data, headers=headers, stream=stream) as resp:
            encoding = 'utf8'
            resp.raise_for_status()
            for chunk in resp.iter_lines():
                if chunk:
                    try:
                        clean_chunk = chunk.decode(encoding).lstrip('data: ').strip()
                        print(clean_chunk)
##                        yield clean_chunk
                        if clean_chunk == "[DONE]":
                            return ''
                        chunk_data = json.loads(clean_chunk)
##                        content = chunk_data['choices'][0]['delta'].get('content', '')
                        event = chunk_data.get('event', '')
                        content = chunk_data.get('text', '') if event == 'cmpl' else ''
                        yield content
                    except (json.JSONDecodeError, KeyError):
                        continue
    except Exception as e:
        yield f"请求失败：{str(e)}"

def respond(message, history,temperature, model):
    print(message, history[-1] if history else history,temperature, model)
    full_response = ""
    for partial in stream_ai_response(message):
        full_response += partial
        yield full_response

with gr.Blocks() as demo:
##    model=gr.Dropdown(['a1', 'a2'], label='模型', value='a1')
    gr.ChatInterface(
        respond,
        chatbot=gr.Chatbot(
            bubble_full_width=False,
            show_copy_button=True
        ),
        textbox=gr.Textbox(placeholder="Ask me a yes or no question", container=False, scale=7),
        additional_inputs=[
            gr.Slider(0, 1, value=0.7, label="Temperature"),
            gr.Dropdown(['a1', 'a2'], label='模型', value='a1')
        ]
    )
if __name__ == "__main__":
    demo.launch(debug=True)