# -*- encoding=utf8 -*-
__author__ = "mu"

from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
import sys

#auto_setup(__file__)

@logwrap
def my_exists(v, threshold=None):
    try:
        pos = loop_find(v, timeout=ST.FIND_TIMEOUT_TMP, threshold=threshold)
    except TargetNotFoundError:
        return False
    else:
        return pos

def get_title():
    obj = poco(desc='返回')
    assert obj.exists(), '没有返回按钮'
    lst = []
    for o in obj.parent().offspring(type='android.widget.TextView'):
        if o.get_position()[0] > 0.7:
            continue
        lst.append(o.get_text())
    return lst

def 返回():
    goback = 0
    find = 0
    title = ''
    for title in get_title():
        if title and title in ('领券中心', '天天点点券'):
            find = 1
            break
    if not find and title:
        log('title:%s goback'%title)
        keyevent("BACK")
        return
    for p in [Template(r"tpl1638187393755.png", record_pos=(-0.004, -0.757), resolution=(720, 1280)),Template(r"tpl1638199959219.png", record_pos=(-0.428, -0.399), resolution=(720, 1280))]:
        if not exists(p):
            keyevent("BACK")
            goback = 1
    log('goback=%d'%goback)

def 领券():
    find = 0
    for p in [Template(r"tpl1638188575389.png", record_pos=(0.004, 0.154), resolution=(720, 1280)), Template(r"tpl1638026934048.png", record_pos=(-0.003, 0.126), resolution=(720, 1280)), Template(r"tpl1638453737190.png", record_pos=(0.0, 0.411), resolution=(720, 1280))]:
        pos = my_exists(p, 0.6)
        if pos:
            touch(pos)
            find = 1
    assert find, '没有找到“领券”入口'

def 处理子任务():
    log('找到一个子任务')
    find = 0
    for title in ['浏览精选商品','领暴力好券', '浏览2组']:
        obj = poco(textMatches='^%s.*$'%title)
        if not obj.exists() or not obj.attr('visible'):
            continue
        txt = obj.get_text()
        log(txt)
        if '/' in txt:
            txt = txt.split('(')[-1].split(')')[0]
            a,b = txt.split('/')
        else:
            b = len(list(obj.parent().child("android.view.View")))
        find = 1
        title = get_title()[0]
        for i in range(b):
            if a == b:
                break
            c = obj.sibling()
            c.click()
            sleep(3)
            while get_title()[0] != title:
                keyevent('BACK')
                sleep(1)
    if find:
        返回()
        return

    sleep(4)
    返回()

def 完成任务():
    while 1:
        pos = 0
        for p in [Template(r"tpl1638027073298.png", record_pos=(0.311, 0.514), resolution=(720, 1280)),Template(r"tpl1638032197678.png", record_pos=(0.312, -0.328), resolution=(720, 1280))]:
            pos = exists(p)
            if pos:
                touch(pos)
                break
        if not pos:
            break
        sleep(3)
        if exists(Template(r"tpl1638031646455.png", record_pos=(-0.003, 0.51), resolution=(720, 1280))):
            log('任务回首页')
            sleep(5)
            for i in range(2):
                领券()
                sleep(22)
                pos = exists(Template(r"tpl1638027067923.png", record_pos=(-0.414, -0.36), resolution=(720, 1280)))
                if not pos:
                    keyevent('BACK')
                else:
                    touch(pos)
            continue
        pos = 0
        for i,p in  enumerate([Template(r"tpl1638372807266.png", record_pos=(0.408, -0.624), resolution=(720, 1280)),Template(r"tpl1638027078131.png", record_pos=(-0.014, -0.301), resolution=(720, 1280)),Template(r"tpl1638338834176.png", record_pos=(0.301, -0.331), resolution=(720, 1280)),Template(r"tpl1638370254899.png", record_pos=(0.304, -0.24), resolution=(720, 1280))]):
            pos = exists(p)
            if pos:
                处理子任务()
                break
        if pos:
            pos = exists(Template(r"tpl1638188203881.png", record_pos=(0.411, -0.543), resolution=(720, 1280)))
            if pos:
                touch(pos) # 要替换掉
                sleep(5)
                continue

        sleep(1)
        返回()
        sleep(1)
        pos = exists(Template(r"tpl1638030477684.png", record_pos=(0.118, 0.819), resolution=(720, 1280)))
        if pos:
            touch(pos)
            sleep(1)
            swipe(Template(r"tpl1638033460814.png", record_pos=(0.289, 0.168), resolution=(720, 1280)), vector=[0.0263, -0.2738])
            sleep(1)

def 收积分():
    下划()

    i = 0
    pos = exists(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
    while pos and i < 10:
        touch(pos)
        sleep(1)
        i += 1
        pos = exists(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
    i = 0
    pos = exists(Template(r"tpl1638027206765.png", record_pos=(-0.132, -0.379), resolution=(720, 1280)))
    while pos and i < 10:
        touch(pos)
        sleep(1)
        i += 1
        pos = exists(Template(r"tpl1638027206765.png", record_pos=(-0.132, -0.379), resolution=(720, 1280)))

def 券后9块9():
    pos = exists(Template(r"tpl1638031232970.png", record_pos=(0.125, 0.824), resolution=(720, 1280)))
    if pos:
        touch(pos)
        sleep(5)
    pos = exists(Template(r"tpl1638026942061.png", record_pos=(-0.018, 0.004), resolution=(720, 1280)))
    if pos:
        touch(pos)
        sleep(5)
        cnt = 0
        for i in range(4):
            pos = exists(Template(r"tpl1638028140053.png", record_pos=(0.115, 0.579), resolution=(720, 1280)))
            if not pos:
                cnt += 1
                if cnt > 2:
                    break
                continue
            cnt = 0
            touch(pos)
            sleep(4)
            pos = exists(Template(r"tpl1638026957412.png", record_pos=(0.032, 0.236), resolution=(720, 1280)))
            if pos:
                touch(pos)
            elif exists(Template(r"tpl1638198505265.png", record_pos=(0.399, -0.414), resolution=(720, 1280))):
                touch(Template(r"tpl1638198505265.png", record_pos=(0.399, -0.414), resolution=(720, 1280)))
            elif exists(Template(r"tpl1638198773456.png", record_pos=(0.433, -0.167), resolution=(720, 1280))):
                touch(Template(r"tpl1638198773456.png", record_pos=(0.433, -0.167), resolution=(720, 1280)))
            else:
                返回()
                sleep(1)
                return 券后9块9()
            sleep(2)
        pos = exists(Template(r"tpl1638454012902.png", record_pos=(0.435, -0.169), resolution=(720, 1280)))
        if pos:
            touch(pos)
            keyevent("BACK")
            sleep(2)
            return
        返回()

def 点折叠上划():
    pos = exists(Template(r"tpl1638030477684.png", record_pos=(0.118, 0.819), resolution=(720, 1280)))
    if pos:
        touch(pos)
    pos = exists(Template(r"tpl1638033460814.png", record_pos=(0.289, 0.168), resolution=(720, 1280)))
    if pos:
        swipe(pos, vector=[0.0263, -0.2738])

def 下划():

    if exists(Template(r"tpl1638033460814.png", record_pos=(0.289, 0.168), resolution=(720, 1280))):
        swipe(Template(r"tpl1638033460814.png", record_pos=(0.289, 0.168), resolution=(720, 1280)), vector=[0.011, 0.5267])
    elif exists(Template(r"tpl1638062275729.png", record_pos=(-0.282, 0.263), resolution=(720, 1280))):
        swipe(Template(r"tpl1638062275729.png", record_pos=(-0.282, 0.263), resolution=(720, 1280)), vector=[0.011, 0.5267])
def 签到():
    if exists(Template(r"tpl1638026989747.png", record_pos=(0.096, -0.113), resolution=(720, 1280))) and not exists(Template(r"tpl1638060827947.png", record_pos=(-0.032, -0.122), resolution=(720, 1280))):
        touch(Template(r"tpl1638026989747.png", record_pos=(0.096, -0.113), resolution=(720, 1280)))
        sleep(3)
        touch(Template(r"tpl1638027002305.png", record_pos=(0.378, -0.439), resolution=(720, 1280)))
        sleep(3)

def 进点点券():
    obj = poco(desc='返回')
    obj = obj.sibling(name='com.jd.lib.coupon.feature:id/dm')
    if obj.exists():
        obj.click()
        return
    for p in [Template(r"tpl1638027006350.png", record_pos=(-0.336, -0.753), resolution=(720, 1280)),Template(r"tpl1638370109774.png", record_pos=(-0.353, -0.768), resolution=(720, 1280)),Template(r"tpl1638188669929.png", record_pos=(0.411, -0.212), resolution=(720, 1280))]:
        pos = exists(p)
        if pos:
            touch(pos)
            return
    assert pos, '找不到点点券入口'


connect_device("Android:///")
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)
touch(Template(r"tpl1638026927082.png", record_pos=(-0.364, -0.353), resolution=(720, 1280)))
sleep(2)
wait(Template(r"tpl1638339769037.png", record_pos=(0.406, -0.664), resolution=(720, 1280)), 30)
领券()
sleep(5)
券后9块9()


touch(Template(r"tpl1638029072323.png", record_pos=(-0.376, 0.826), resolution=(720, 1280)))
sleep(3)

签到()
进点点券()
sleep(3)
点折叠上划()

for i in range(2):
    完成任务()
    收积分()
keyevent("BACK")
keyevent("BACK")
keyevent("BACK")
keyevent("BACK")
sys.exit(0)

####################################################################



touch(Template(r"tpl1638027014984.png", record_pos=(0.021, 0.518), resolution=(720, 1280)))
touch(Template(r"tpl1638027020047.png", record_pos=(0.01, 0.138), resolution=(720, 1280)))

touch(Template(r"tpl1638027073298.png", record_pos=(0.311, 0.514), resolution=(720, 1280)))

touch(Template(r"tpl1638027083659.png", record_pos=(0.182, -0.246), resolution=(720, 1280)))
返回()

返回()

touch(Template(r"tpl1638027107226.png", record_pos=(0.029, 0.331), resolution=(720, 1280)))
touch(Template(r"tpl1638027109645.png", record_pos=(0.339, 0.224), resolution=(720, 1280)))
touch(Template(r"tpl1638027111116.png", record_pos=(-0.011, 0.432), resolution=(720, 1280)))
touch(Template(r"tpl1638027116317.png", record_pos=(-0.19, 0.118), resolution=(720, 1280)))
返回()
touch(Template(r"tpl1638027124138.png", record_pos=(0.013, 0.138), resolution=(720, 1280)))
touch(Template(r"tpl1638027135500.png", record_pos=(-0.394, -0.344), resolution=(720, 1280)))
touch(Template(r"tpl1638027073298.png", record_pos=(0.311, 0.514), resolution=(720, 1280)))
返回()
touch(Template(r"tpl1638027073298.png", record_pos=(0.311, 0.514), resolution=(720, 1280)))
返回()
touch(Template(r"tpl1638027180072.png", record_pos=(0.315, 0.514), resolution=(720, 1280)))
返回()
touch(Template(r"tpl1638027073298.png", record_pos=(0.311, 0.514), resolution=(720, 1280)))
返回()
touch(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
touch(Template(r"tpl1638027205091.png", record_pos=(0.307, -0.26), resolution=(720, 1280)))
touch(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
touch(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
touch(Template(r"tpl1638027206174.png", record_pos=(0.031, -0.4), resolution=(720, 1280)))
touch(Template(r"tpl1638027208591.png", record_pos=(-0.347, -0.014), resolution=(720, 1280)))
返回()
返回()
