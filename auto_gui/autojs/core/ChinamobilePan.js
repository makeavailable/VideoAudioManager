/**
 * 云盘签到
 */
let { config } = require('../config.js')(runtime, global)
let singletonRequire = require('../lib/SingletonRequirer.js')(runtime, this)
let FloatyInstance = singletonRequire('FloatyUtil')
let widgetUtils = singletonRequire('WidgetUtils')
let automator = singletonRequire('Automator')
let commonFunctions = singletonRequire('CommonFunction')
let localOcrUtil = require('../lib/LocalOcrUtil.js')
let logUtils = singletonRequire('LogUtils')
let BaseSignRunner = require('./BaseSignRunner.js')

function isPositionValid(x, y) {
  if (x < 0 || y < 0 || x >= config.device_width || y >= config.device_height) {
    return false
  }
  return true
}

function SignRunner () {
  BaseSignRunner.call(this)
  this.panConfig = config.yd_pan_config
  this.subTasks = config.supported_signs.filter(task => {
    console.log(task.taskCode)
    return ['ChinamobilePan', 'Chinamobile'].includes(task.taskCode)
  })
  this.subTasks = this.subTasks.length > 0 ? this.subTasks[0].subTasks : []
  const ChinamobilePan = this.subTasks.filter(task => task.taskCode == 'ChinamobilePan')
  const aliPan = this.subTasks.filter(task => task.taskCode == 'aliPan')

  this.yidongYunPan = function() {
    if (ChinamobilePan.length > 0 && this.isSubTaskExecuted(ChinamobilePan[0])) {
      return
    }
    let _package_name = 'com.chinamobile.mcloud'
    launch(_package_name)
    sleep(1000)
    this.awaitAndSkip(['\\s*允许\\s*', '\\s*取消\\s*', /.*版本升级.*/])    
    let lst = [
      {
        id: /.*?title_right_opt_2/,
        region: [0,0,config.device_width, config.device_height * 0.2],
        text: '签',
        notFound: function() {
          let clickMine = widgetUtils.widgetGetOne('我的工具')
          if (clickMine) {
            clickMine = clickMine.parent()
            clickMine = clickMine.childCount()>0?clickMine.child(clickMine.childCount()-1):null
          }
          return clickMine
        },
        waitFor: ['做任务.*','周一']
      },
      {
        text: /^\+\d+.*/,
        loopFilter: (node) => {
          let txt = node.text?node.text():node.label
          return !txt.includes('下月') && automator.checkCenterClickable(node)
        },
        boundsInside: [0,config.device_height * 0.2,config.device_width, config.device_height * 0.7],
        waitFor: ['.*知道.*', '周一']
      },
    ]
    this.pipeLine(lst)
    if (lst[0].find) {      
      ChinamobilePan.length > 0 && this.setSubTaskExecuted(ChinamobilePan[0])
    }
    logUtils.debugInfo(['移动云盘结束'])
    automator.back()
    automator.back()
    sleep(3000)
    !config._debugging && commonFunctions.minimize(_package_name)
  }

  this.aliPan = function() {
    if (aliPan.length > 0 && this.isSubTaskExecuted(aliPan[0])) {
      return
    }
    let _package_name = 'com.alicloud.databox'
    launch(_package_name)
    sleep(1000)
    this.awaitAndSkip(['\\s*允许\\s*', '\\s*取消\\s*'])
    let lst = [
      {
        text: '^我的$',
        region: [0,config.device_height * 0.8,config.device_width, config.device_height]
      },
      {
        text: '^福利社$',
        region: [0,config.device_height*0.5,config.device_width, config.device_height*0.8]
      },
      {
        text: '^每日签到.*',
        region: [0,0,config.device_width, config.device_height*0.5]
      },
      {
        text: '^领取$',
        region: [0,config.device_height*0.2,config.device_width, config.device_height*0.7],
        point: this.panConfig?this.panConfig.lingQu:{}
      },
    ]
    this.pipeLine(lst)
    for (let info of lst.reverse()) {
      if (info.find) {
        automator.back()
        sleep(1000)
      }
    }
    if (lst[lst.length-1].find) {      
      aliPan.length > 0 && this.setSubTaskExecuted(aliPan[0])
    }
    sleep(3000)
    !config._debugging && commonFunctions.minimize(_package_name)
  }

  this.exec = function () {
    this.yidongYunPan()
    this.aliPan()
    this.setExecuted()
  }
}

SignRunner.prototype = Object.create(BaseSignRunner.prototype)
SignRunner.prototype.constructor = SignRunner
module.exports = new SignRunner()