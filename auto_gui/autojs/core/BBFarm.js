/*
 * @Author: TonyJiangWJ
 * @Date: 2020-04-25 16:46:06
 * @Last Modified by: TonyJiangWJ
 * @Last Modified time: 2023-07-19 23:38:40
 * @Description: 
 */

let { config } = require('../config.js')(runtime, this)
let singletonRequire = require('../lib/SingletonRequirer.js')(runtime, this)
let commonFunctions = singletonRequire('CommonFunction')
let alipayUnlocker = singletonRequire('AlipayUnlocker')
let widgetUtils = singletonRequire('WidgetUtils')
let logUtils = singletonRequire('LogUtils')
let automator = singletonRequire('Automator')
let FloatyInstance = singletonRequire('FloatyUtil')
let BaseSignRunner = require('./BaseSignRunner.js')

function SignRunner () {
  BaseSignRunner.call(this)

  let bb_farm_config = config.bb_farm_config

  this.exec = function () {
    this.executeAlipayFarm()
    this.setExecuted()
  }

  this.executeAlipayFarm = function () {
    FloatyInstance.setFloatyPosition(400, 400)
    FloatyInstance.setFloatyText('准备打开支付宝芭芭农场页面')
    this.openAlipayFarm()
    let region = [config.device_width/2, config.device_height/2, config.device_width/2, config.device_height/2]
    let clicked = false
    while (1) {
      if (this.captureAndCheckByOcr(/点击领取/, '点击领取', region, null, true)) {
        clicked = true
        break
      }
      if (this.captureAndCheckByOcr(/明日.*领/, '已经领取:明日.*领', region, null, false)) {
        break
      }
      if (this.captureAndCheckByImg(bb_farm_config.collect_btn_alipay, '每日签到', null, true)) {
        clicked = true
        break
      }
      let btn = widgetUtils.widgetGetOne(/任务列表/,1000)
      if (btn) {
        let x = btn.bounds().centerX()
        let y = btn.bounds().centerY()-300
        automator.click(x, y)
        FloatyInstance.setFloatyInfo({x,y}, '点击领取')
        clicked = true
        break
      }
      break
    }
    let options = { boundsInside: [0, config.device_height*0.3, config.device_width, config.device_height*0.8] }
    clicked?this.awaitAndSkip(['关闭'], options):false
    sleep(1000)
    // 签到任务
    this.collectAlipayTask()
    // TODO 更多的逛一逛任务
    // 打开
    this.executeTaobaoFarm()

    commonFunctions.minimize('com.eg.android.AlipayGphone')
  }

  this.openAlipayFarm = function () {

    let _package_name = 'com.eg.android.AlipayGphone'
    if (config.is_alipay_locked) {
      commonFunctions.launchPackage(_package_name)
      sleep(500)
      alipayUnlocker.unlockAlipay()
    }
    app.startActivity({
      action: 'VIEW',
      // data: 'alipays://platformapi/startapp?appId=68687599&url=%2Fwww%2FmyPoints.html',
      data: 'https://render.alipay.com/p/s/i/?scheme=alipays%3A%2F%2Fplatformapi%2Fstartapp%3FappId%3D68687599%26source%3Dnormalshare%26chInfo%3Dch_share__chsub_CopyLink%26fxzjshareChinfo%3Dch_share__chsub_CopyLink%26apshareid%3D82a6fd96-1f82-4975-bb8c-ac87e8aedfba%26shareBizType%3DBABAFarm',
      packageName: _package_name
    })
    sleep(500)
    FloatyInstance.setFloatyText('校验是否有打开确认弹框')
    let confirm = widgetUtils.widgetGetOne(/^打开$/, 3000)
    if (confirm) {
      this.displayButtonAndClick(confirm, '找到了打开按钮')
    } else {
      FloatyInstance.setFloatyText('没有打开确认弹框')
    }

    this.checkForTargetImg(bb_farm_config.entry_check_alipay, '农场加载校验')
  }

  this.collectAlipayTask = function () {
    while(1) {
      let collect = widgetUtils.widgetGetOne(/任务列表/,1000)
      if (collect) {
        this.displayButtonAndClick(collect, '通过控件找到任务入口')
        break
      }
      if (this.captureAndCheckByImg(bb_farm_config.task_btn_alipay, '每日任务', null, true)) {
        break
      }
      FloatyInstance.setFloatyText('通过图片查找每日任务入口失败')
      let region = [config.device_width/2, config.device_height/2, config.device_width/2, config.device_height/2]
      if (this.captureAndCheckByOcr('领肥料', '每日任务入口：领肥料', region, null, true)||
      this.captureAndCheckByOcr('加量', '每日任务入口：加量', region, null, true)) {
        logUtils.debugInfo(['通过OCR查找入口成功'])
        FloatyInstance.setFloatyText('找到任务入口')
        break
      }
      logUtils.debugInfo(['坏了，没找到任务入口'])
      break
    }
    sleep(1000)
    let collect = widgetUtils.widgetGetOne('^领取$')
    while (collect) {
      collect.click()
      sleep(500)
      collect = widgetUtils.widgetGetOne('^领取$')
    }
  }

  this.executeTaobaoFarm = function () {
    // this.launchTaobao()
    let entry = widgetUtils.widgetGetOne('前往手机淘宝-芭芭农场')
    if (entry) {
      FloatyInstance.setFloatyInfo({x:config.device_width/2, y:config.device_height/2},'打开淘宝芭芭农场')
      sleep(1000)
      entry.click()
      sleep(1000)

      let welcomBtn = widgetUtils.widgetGetOne('继续努力')
      if (welcomBtn) {
        automator.clickCenter(welcomBtn)
        sleep(1000)
      }

      this.checkForTargetImg(bb_farm_config.entry_check_taobao, '农场加载校验')

      while (1) {
        if (this.captureAndCheckByImg(bb_farm_config.collect_btn_taobao, '每日签到', null, true)) {
          FloatyInstance.setFloatyText('签到by captureAndCheckByImg')
          logUtils.debugInfo(['签到by captureAndCheckByImg'])
          break
        }
        let rect = [0,config.device_height*0.5, config.device_width, config.device_height*0.3]
        let point = null
        let find = false
        while ((point=this.captureAndCheckByOcr(/点击领取/,'点击领取', rect, null, true))) {
          FloatyInstance.setFloatyInfo(this.boundsToPosition(point), '签到by captureAndCheckByOcr')
          logUtils.debugInfo(['签到by captureAndCheckByOcr'])
          this.awaitAndSkip(['关闭', '我知道了'])
          find = true
          while (!widgetUtils.widgetGetOne(/施肥.*次/, 1000)) {
            logUtils.debugInfo(['back'])
            automator.back()
            sleep(1000)
          }
        }
        if (find) break
        logUtils.debugInfo(['没有找到领取'])
        break
      }
      this.collectTaobaoTask()

      commonFunctions.minimize('com.taobao.taobao')
    }
  }

  this.collectTaobaoTask = function () {
    while (1) {
      let point = widgetUtils.widgetGetOne(/集肥料/, 1000)
      if (point) {
        this.displayButtonAndClick(point, '通过控件找到任务入口')
        break
      }
      if ((point = this.captureAndCheckByImg(bb_farm_config.task_btn_taobao, '每日任务', null, true) )) {
        FloatyInstance.setFloatyInfo(this.boundsToPosition(point), '签到by captureAndCheckByOcr')
        logUtils.debugInfo(['签到by captureAndCheckByOcr'])
        break
      }
      logUtils.debugInfo(['没打开任务列表'])
      break
    }
    sleep(1000)
    let collect = widgetUtils.widgetGetOne('去签到')
    if (collect) {
      FloatyInstance.setFloatyInfo({ x: collect.bounds().x, y: collect.bounds().y }, '去签到')
      collect.click()
      sleep(3000)
    }
  }

  this.launchTaobao = function () {
    let _package_name = 'com.taobao.taobao'
    app.launch(_package_name)
    sleep(500)
    FloatyInstance.setFloatyText('校验是否有打开确认弹框')
    let confirm = widgetUtils.widgetGetOne(/^打开|允许$/, 3000)
    if (confirm) {
      this.displayButtonAndClick(confirm, '找到了打开按钮')
    } else {
      FloatyInstance.setFloatyText('没有打开确认弹框')
    }
  }
}

SignRunner.prototype = Object.create(BaseSignRunner.prototype)
SignRunner.prototype.constructor = SignRunner

module.exports = new SignRunner()
