let singletonRequire = require('../lib/SingletonRequirer.js')(runtime, this)
let commonFunctions = singletonRequire('CommonFunction')
let widgetUtils = singletonRequire('WidgetUtils')
let automator = singletonRequire('Automator')
let FloatyInstance = singletonRequire('FloatyUtil')
let logUtils = singletonRequire('LogUtils')

let BaseSignRunner = require('./BaseSignRunner.js')
function SignRunner () {
  BaseSignRunner.call(this)
  const _package_name = 'com.taobao.taobao'

  this.launchTaobao = function () {
    app.launch(_package_name)
    sleep(500)
    FloatyInstance.setFloatyText('校验是否有打开确认弹框')
    let confirm = widgetUtils.widgetGetOne(/^打开|允许$/, 3000)
    if (confirm) {
      this.displayButtonAndClick(confirm, '找到了打开按钮')
    } else {
      FloatyInstance.setFloatyText('没有打开确认弹框')
    }
  }

  this.checkAndCollect = function () {
    let lst = [
      {
        text: '领淘金币',
        waitFor: ['已连签.*']
      },
      {
        region: [0,0,config.device_width, config.device_height * 0.3],
        text: '.*(今日签到|明日签到).*',
        waitFor: ['关闭', '已连签.*']
      },
      {
        text: '.*首页来访',
        waitFor: ['关闭', '已连签.*']
      },
    ]
    this.pipeLine(lst)
    if (lst[0].find) {
      this.setExecuted()
    }
  }

  this.launchTown = function () {
    app.startActivity({
      action: 'android.intent.action.VIEW',
      data: 'taobao://pages.tmall.com/wow/z/tmtjb/town/task',
      packageName: _package_name
    })
    sleep(500)
  }

  this.exec = function () {
    this.launchTaobao(_package_name)
    sleep(1000)
    this.checkAndCollect()
    sleep(1000)
    commonFunctions.minimize(_package_name)
  }
}

SignRunner.prototype = Object.create(BaseSignRunner.prototype)
SignRunner.prototype.constructor = SignRunner

module.exports = new SignRunner()