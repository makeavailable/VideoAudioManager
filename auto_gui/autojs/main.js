const VERSION = '20221022-1'

let showVersion = function () {
    console.log('版本：' + VERSION)
    toast('当前版本：' + VERSION)
}

console.log('脚本更新地址：')
console.log('https://github.com/ashuang360/jd-tb')

if (!auto.service) {
    toast('无障碍服务未启动！退出！')
    exit()
}
//threads.start(function () {
//    var beginBtn;
//       if (beginBtn = classNameContains("Button").textContains("立即开始").findOne(2000)) {
//           beginBtn.click();
//       }
//})         
 
//if (!requestScreenCapture()) {
//    toastLog('请求截图权限失败')
//    exit()
//}
function getSetting() {
    let indices = []
    autoOpen && indices.push(0)
    autoMute && indices.push(1)
    autoJoin && indices.push(2)
    autoConsume && indices.push(3)
    autoUpdate && indices.push(4)
    indices.push(5)
 
    let settings = dialogs.multiChoice('任务设置', 
        [
            '自动打开京东进入活动。多开或任务列表无法自动打开时取消勾选', 
            '自动调整媒体音量为0。以免直播任务发出声音，首次选择需要修改系统设置权限', 
            '自动完成入会任务。京东将授权手机号给商家，日后可能会收到推广短信', 
            '自动抽奖',
            '自动更新代码',
            '此选项用于保证选择的处理，勿动！'
        ], indices)
 
    if (settings.length == 0) {
        toast('取消选择，任务停止')
        exit()
    }
 
    if (settings.indexOf(0) != -1) {
        storage.put('autoOpen', true)
        autoOpen = true
    } else {
        storage.put('autoOpen', false)
        autoOpen = false
    }
    if (settings.indexOf(1) != -1) {
        storage.put('autoMute', true)
        autoMute = true
    } else {
        storage.put('autoMute', false)
        autoMute = false
    }
    if (settings.indexOf(2) != -1) {
        storage.put('autoJoin', true)
        autoJoin = true
    } else {
        storage.put('autoJoin', false)
        autoJoin = false
    }
    if (settings.indexOf(3) != -1) {
        storage.put('autoConsume', true)
        autoConsume = true
    } else {
        storage.put('autoConsume', false)
        autoConsume = false
    }
    if (settings.indexOf(4) != -1) {
        storage.put('autoUpdate', true)
        autoUpdate = true
    } else {
        storage.put('autoUpdate', false)
        autoUpdate = false
    }
}
 
let storage = storages.create("jd_task");
let autoOpen = storage.get('autoOpen', true)
let autoMute = storage.get('autoMute', false)
let autoJoin = storage.get('autoJoin', false)
let autoConsume = storage.get('autoConsume', false)
let autoUpdate = storage.get('autoUpdate', false)
let closePopupRegx = /关闭(弹窗|页面)/

// getSetting()
 
if (autoMute) {
    try {
        device.setMusicVolume(0)
        toast('成功设置媒体音量为0')
    } catch (err) {
        alert('首先需要开启权限，请开启后再次运行脚本')
        exit()
    }
}

if (autoUpdate) {
    let r = http.get("https://gitee.com/makeavailable/VideoAudioManager/raw/master/auto_gui/autojs/main.js");
    let path = files.cwd() + '/main.js'
    files.write(path, r.body.string())
    quit()
}
 
showConsole(1)
console.log('开始完成京东任务...')
console.log('按音量下键停止')
 
device.keepScreenDim(30 * 60 * 1000) // 防止息屏30分钟
 
// 自定义取消亮屏的退出方法
function quit() {
    screenShotInNeight()
    device.cancelKeepingAwake()
    exit()
}
 
// 监听音量下键
function registerKey() {
    try {
        events.observeKey()
    } catch (err) {
        console.log('监听音量键停止失败，应该是无障碍权限出错，请关闭软件后台任务重新运行。')
        console.log('如果还是不行可以重启手机尝试。')
        quit()
    }
    events.onKeyDown('volume_down', function (event) {
        console.log('京东任务脚本停止了')
        console.log('请手动切换回主页面')
        startCoin && console.log('本次任务开始时有' + startCoin + '金币')
        quit()
    })
}
threads.start(registerKey)
 
// 自定义一个findTextDescMatchesTimeout
function findTextDescMatchesTimeout(reg, timeout) {
    let c = 0
    let result = 0
    let tp = typeof reg
    while (c < timeout / 50) {
        if (tp == 'string') {
            result = text(reg).findOnce() || desc(reg).findOnce()
        } else {
            result = textMatches(reg).findOnce() || descMatches(reg).findOnce()
        }
        if (result) return result
        sleep(50)
        c++
    }
    return null
}
let g_pkgName = 'com.jingdong.app.mall'
function openApp(pkg, msg) {
    if (!pkg) pkg = g_pkgName
    if (!msg) msg = /首页/
    if(findTextDescMatchesTimeout(msg, 2000)) return
    let obj = findTextDescMatchesTimeout(/.*/, 1000)
    if (!obj || obj.packageName().indexOf(pkg) < 0){
        console.log('正在打开App...', pkg, obj.packageName())
        if (!launch(pkg)) {
            console.log('可能未安装App:', pkg)
        }
        if (device.model == 'Redmi Note 4X' && pkg.indexOf('jingdong') > -1) {
            let aaa = '/' + app.getAppName(pkg) + '/'
            clickText(eval(aaa))
        }
        findTextDescMatchesTimeout(msg, 20000)
    }
}

// 打开京东进入活动
function openAndInto(txt, pkg) {
    openApp(pkg)
    backToMain()
    if (!txt) {
        console.log('进入活动页面')
        //app.startActivity({
         //   action: "VIEW",
         //   data: 'openApp.jdMobile://virtual?params={"category":"jump","action":"to","des":"m","sourceValue":"JSHOP_SOURCE_VALUE","sourceType":"JSHOP_SOURCE_TYPE","url":"https://u.jd.com/kIrrQ3H","M_sourceFrom":"mxz","msf_type":"auto"}'
        //})
        enterAction()
        return
    }
    clickText(txt)
}
 
// 获取金币数量
function getCoin() {
    let anchor = text('消耗').findOne(5000)
    if (!anchor) {
        console.log('找不到消耗控件')
        return false
    }
    let coin = anchor.parent().parent().parent().parent().child(1).text()
    if (coin) {
        return parseInt(coin)
    } else {
        coin = anchor.parent().parent().parent().parent().child(2).text() // 有可能中间插了个控件
        if (coin) {
            return parseInt(coin)
        } else {
            return false
        }
    }
}
 
// 打开任务列表
function openTaskList() {
    console.log('打开任务列表')
    let taskListButtons = text('消耗').findOne(20000)
    let i = 2
    while (i--) {
        if (!taskListButtons) {
            backToMain()
            enterAction()
            taskListButtons = text('消耗').findOne(20000)
            continue
        }
        break
    }
    taskListButtons = taskListButtons.parent().parent().parent().parent().children()
 
    let taskListButton = taskListButtons.findOne(boundsInside(device.width/2, 0, device.width, device.height).clickable())
 
    if (!taskListButton || !taskListButton.clickable()) {
        console.log('无法找到任务列表控件')
        quit()
    }
    taskListButton.click()
    console.log('等待任务列表')
    if (!findTextDescMatchesTimeout(/累计任务奖励/, 5000)) {
        console.log('似乎没能打开任务列表，重试')
        taskListButton.click()
    }
 
    if (!findTextDescMatchesTimeout(/累计任务奖励/, 10000)) {
        console.log('似乎没能打开任务列表，退出！')
        console.log('如果已经打开而未检测到，请删除101版本及以上的webview或使用国内应用市场版京东尝试。')
        quit()
    }
}
 
// 关闭任务列表
function closeTaskList() {
    console.log('关闭任务列表')
    let renwu = findTextDescMatchesTimeout(/.*做任务.*/, 5000)
    if (!renwu) {
        console.log('无法找到任务奖励标识')
        return false
    }
    let closeBtn = renwu.parent().parent().parent().child(0)
    return closeBtn.click()
}
 
// 重新打开任务列表
function reopenTaskList() {
    closeTaskList()
    sleep(3000)
    openTaskList()
    sleep(5000)
}
 
// 获取未完成任务，根据数字标识，返回任务按钮、任务介绍、任务数量（数组）
let taskCount = {}
function getTaskByText() {
    let tButton = null,
        tText = null,
        tCount = 0,
        tTitle = null
    console.log('寻找未完成任务...')
    let taskButtons = textMatches(/去完成|去领取|去打卡/).find()
    if (!taskButtons.empty()) { // 如果找不到任务，直接返回
        for (let i = 0; i < taskButtons.length; i++) {
            let button = taskButtons[i]
            // if (tButton.text() == '去领取') {
            //     console.log('领取奖励')
            //     tButton.click()
            //     sleep(500)
            //     continue
            // }
            if (button.indexInParent() == 0) continue
            let tmp = 0
            try {
                tmp =button.parent().child(button.indexInParent()-1)
                if (tmp.childCount() == 0) continue
                tTitle = tmp.child(0).text()
            } catch(err) {
                console.log(button)
                throw err 
            }
            let r = tTitle.match(/(\d*)\/(\d*)/)
            if (!r) continue
 
            tCount = (r[2] - r[1])
 
            console.log(tTitle, tCount)
            if (tCount) { // 如果数字相减不为0，证明没完成
                tText = tmp.child(1).text()
                if (!autoJoin && tText.match(/成功入会|小程序/)) continue
                if (tTitle.match(/下单|小程序/)) continue
                let cnt = taskCount[tTitle] || 0
                if (cnt > 2) {
                    continue
                }
                taskCount[tTitle] = (cnt + 1)
                tButton = button
                break
            }
        }
    } else {
        console.log('任务提示未找到')
    }
    return [tButton, tText, tCount, tTitle]
}
 
// 返回任务列表并检查是否成功，不成功重试一次，带有延时
function backToList() {
    backToText(/累计任务奖励/)
}
 
// 浏览n秒的任务
function timeTask() {
    console.log('等待浏览任务完成...')
    if (textMatches(/.*滑动浏览.*[^可]得.*/).findOne(10000)) {
        sleep(1000)
        console.log('模拟滑动')
        swipe(device.width / 2, device.height - 200, device.width / 2 + 20, device.height - 500, 1000)
    }
    let c = 0
    while (c < 40) { // 0.5 * 40 = 20 秒，防止死循环
        if ((textMatches(/获得.*?金币/).exists() || descMatches(/获得.*?金币/).exists())) // 等待已完成出现
            break
        if ((textMatches(/已浏览/).exists() || descMatches(/已浏览/).exists())) { // 失败
            console.log('上限，返回刷新任务列表')
            return false
        }
 
        // 弹窗处理
        let pop = text('升级开卡会员领好礼')
        if (pop.exists()) {
            pop.findOnce().parent().parent().child(2).click()
            console.log('关闭会员弹窗')
        }
 
        sleep(500)
        c++
    }
    if (c > 39) {
        console.log('未检测到任务完成标识。')
        return false
    }
    console.log('已完成，准备返回')
    return true
}
 
// 入会任务
function joinTask() {
    let check = textMatches(/.*确认授权即同意.*|.*我的特权.*|.*立即开卡.*|.*解锁全部会员福利.*/).findOne(8000)
    if (!check) {
        console.log('无法找到入会按钮，判定为已经入会')
        return true
    } else if (check.text().match(/我的特权/)) {
        console.log('已经入会，返回')
        return true
    } else {
        sleep(2000)
        if (check.text().match(/.*立即开卡.*|.*解锁全部会员福利.*|授权解锁/)) {
            let btn = check.bounds()
            console.log('即将点击开卡/解锁福利，自动隐藏控制台')
            sleep(500)
            showConsole(0)
            sleep(500)
            click(btn.centerX(), btn.centerY())
            sleep(500)
            showConsole(1)
            sleep(5000)
            check = textMatches(/.*确认授权即同意.*/).boundsInside(0, 0, device.width, device.height).findOne(8000)
        }
 
        if (!check) {
            console.log('无法找到入会按钮弹窗，加载失败')
            return false
        }
 
        // text("instruction_icon") 全局其实都只有一个, 保险起见, 使用两个parent来限定范围
        let checks = check.parent().parent().find(text("instruction_icon"));
        if (checks.size() > 0) {
            // 解决部分店铺(欧莱雅)开卡无法勾选 [确认授权] 的问题           
            check = checks.get(0);
        } else {
            if (check.indexInParent() >= 6) {
                check = check.parent().child(5)
            } else if (check.text() == '确认授权即同意') {
                check = check.parent().child(0)
            } else {
                check = check.parent().parent().child(5)
            }
        }
 
        check = check
        log("最终[确认授权]前面选项框坐标为:", check);
        let x = check.bounds().centerX()
        let y = check.bounds().centerY()
 
        console.log('检测是否有遮挡')
        let float = className('android.widget.ImageView')
            .filter(function (w) {
                let b = w.bounds()
                return b.left <= x && b.right >= x && b.top <= y && b.bottom >= y && b.centerX() != x && b.centerY() != y
            }).findOnce()
 
        if (float) {
            console.log('有浮窗遮挡，尝试移除')
            if (device.sdkInt >= 24) {
                gesture(1000, [float.bounds().centerX(), float.bounds().centerY()], [float.bounds().centerX(), y + float.bounds().height()])
                console.log('已经进行移开操作，如果失败请反馈')
            } else {
                console.log('安卓版本低，无法自动移开浮窗，入会任务失败。至少需要安卓7.0。')
                return false
            }
        } else {
            console.log('未发现遮挡的浮窗，继续勾选')
        }
 
        console.log('即将勾选授权，自动隐藏控制台')
        sleep(500)
        showConsole(0)
        sleep(1000)
        click(x, y)
        sleep(500)
        showConsole(1)
 
        console.log('准备点击入会按钮')
        let j = textMatches(/^确认授权(并加入店铺会员)*$|.*立即开通.*/).findOne(5000)
        if (!j) {
            console.log('无法找到入会按钮，失败')
            return false
        }
        click(j.bounds().centerX(), j.bounds().centerY())
        sleep(1000)
        console.log('入会完成，返回')
        return true
    }
}
 
// 浏览商品和加购的任务，cart参数为是否加购的flag
function itemTask(cart) {
    console.log('等待进入商品列表...')
    if (!textContains('当前页').findOne(10000)) {
        console.log('未能进入商品列表。')
        return false
    }
    sleep(2000)
    let items = textContains('.jpg!q70').find()
    for (let i = 0; i < items.length; i++) {
        if (cart) {
            console.log('加购并浏览')
            let tmp = items[i].parent().parent()
            tmp.child(tmp.childCount() - 1).click()
        } else {
            console.log('浏览商品页')
            items[i].parent().parent().child(4).click()
        }
        sleep(5000)
        console.log('返回')
        back()
        sleep(5000)
        let r = textContains('.jpg!q70').findOnce()
        if (!r) {
            back()
            sleep(5000)
        }
        if (i >= 4 - 1) {
            break
        }
    }
    return true
}
 
// 逛店任务
function shopTask() {
    console.log('等待进入店铺列表...')
    let banner = textContains('喜欢').findOne(10000)
    if (!banner) {
        console.log('未能进入店铺列表。返回。')
        return false
    }
    let c = banner.text().match(/(\d)\/(\d*)/)
    if (!c) {
        c = 4 // 进行4次
    } else {
        c = c[2] - c[1]
    }
    sleep(2000)
    console.log('进行', c, '次')
    let like = textContains('喜欢').boundsInside(1, 0, device.width, device.height).findOnce()
    if (!like) {
        console.log('未能找到喜欢按钮。返回。')
        return false
    }
    let bound = [like.bounds().centerX(), like.bounds().centerY()]
    console.log('喜欢按钮位于', bound)
    for (let i = 0; i < c; i++) {
        click(bound[0], bound[1])
        console.log('浏览店铺页')
        sleep(8000)
        console.log('返回')
        back()
        sleep(5000)
        let r = textContains('喜欢').findOnce()
        if (!r) {
            back()
            sleep(5000)
        }
    }
    return true
}
 
// 参观任务
function viewTask() {
    console.log('进行参观任务')
    sleep(5000)
    console.log('参观任务直接返回')
    return true
}
 
// 品牌墙任务
function wallTask() {
    console.log('进行品牌墙任务')    
    swipe(device.width / 2, device.height - 200, device.width / 2 + 20, 200, 1000)
    sleep(3000)
    let r = textContains('到底了').findOne(8000)
    if (!r) {
        swipe(device.width / 2 + 20, 200, device.width / 2, device.height - 200, 1000)
        openAndInto()
        enterAction()
        return false
    }
    slptm = 8000
    let x = 415, y = device.height - 255
    for (let i of [1, 2, 3, 4, 5]) { // 选5个
        console.log('打开一个')
        // textContains('!q70').boundsInside(0, 0, device.width, device.height).findOnce(i).click()
        click(x, y)
        if (i !== 1) slptm = 4000
        y = y - 200
        sleep(slptm)
        console.log('直接返回')
        back()
        let r = textContains('到底了').findOne(8000)
        if (!r) back()
        sleep(2000)
    }
    // console.log('返回顶部')
    // let root = textContains('到底了').findOnce().parent().parent()
    // root.child(root.childCount() - 2).click()
    backToList()
    console.log('品牌墙完成后重新打开任务列表')
    sleep(3000)
    openTaskList()
    clearRecentApp(false)
    return true
}
 
// 单个任务的function，自动进入任务、自动返回任务列表，返回boolean
function doTask(tButton, tText, tTitle) {
    let clickFlag = tButton.click()
    let tFlag
    sleep(1000)
    if (tButton.text() == '去领取'|| findTextDescMatchesTimeout(/累计任务奖励/, 1000)) {
        tFlag = clickFlag // 打卡点击一次即可
        return tFlag
    }
 
    if (tText.match(/浏览并关注.*s|浏览.*s/)) {
        console.log('进行', tText)
        tFlag = timeTask()
    } else if (tText.match(/累计浏览/)) {
        console.log('进行累计浏览任务')
        if (tText.match(/加购/)) {
            tFlag = itemTask(true)
        } else {
            tFlag = itemTask(false)
        }
    } else if (tText.match(/入会/)) {
        console.log('进行入会任务')
        tFlag = joinTask()
    } else if (tText.match(/浏览可得|浏览并关注|晚会/)) {
        if (tTitle.match(/种草城/)) {
            tFlag = shopTask()
        } else {
            tFlag = viewTask()
        }
    } else if (tText.match(/品牌墙/)) {
        if (tTitle.match(/浏览更多权益/)) {
            console.log('简单品牌墙任务，等待10s')
            sleep(10000)
            return true
        } 
        tFlag = wallTask()
        return tFlag // 品牌墙无需backToList，提前返回
    } else if (tText.match(/打卡|首页/)) {
        tFlag = clickFlag // 打卡点击一次即可
        return tFlag
    } else if (tText.match(/组队/)) {
        console.log('等待组队任务')
        sleep(3000)
        if (findTextDescMatchesTimeout(/累计任务奖励/, 1000)) {
            console.log('当前仍在任务列表，说明已经完成任务且领取奖励，返回')
            return true
        } else {
            if (textContains('我的金币').findOne(10000)) {
                console.log('进入到组队页面，返回')
                backToList()
                console.log('等待领取奖励')
                sleep(2000)
                tFlag = tButton.click()
                sleep(2000)
                return tFlag
            } else {
                console.log('未能进入组队')
                console.log('组队任务失败，避免卡死，退出')
                quit()
            }
        }
    } else {
        console.log('未知任务类型，默认为浏览任务', tText)
        tFlag = timeTask()
    }
    backToList()
    return tFlag
}
 
function signTask() {
    let anchor = className('android.view.View').filter(function (w) {
        return w.clickable() && (w.text() == '去使用奖励' || w.desc() == '去使用奖励')
    }).findOne(5000)
 
    if (!anchor) {
        console.log('未找到使用奖励按钮，签到失败')
        return false
    }
 
    let anchor_index = anchor.indexInParent()
    let sign = anchor.parent().child(anchor_index + 2) // 去使用的后两个
    sign.click()
    sleep(3000)
 
    sign = textMatches(/.*立即签到.*|.*明天继续来.*/).findOne(5000)
    if (!sign) {
        console.log('未找到签到按钮')
        return false
    }
 
    if (sign.text().match(/明天继续来/)) {
        console.log('已经签到')
    } else {
        sign.click()
    }
 
    return true
}
 
// 领取金币
function havestCoin() {
    console.log('准备领取自动积累的金币')
    let h = clickText(/.*领取金币.*|.*后满.*/)
    if (h) {
        console.log('领取成功')
    } else { console.log('未找到金币控件，领取失败') }
}
let centerX = 0, centerY = 0
function enterAction() {
    let h = descMatches(/.*浮层活动.*/).findOne(5000)
    if (h) {
        h.click()
        if (centerY == 0 ) {
            centerX = h.bounds().centerX()
            centerY = h.bounds().centerY()
        }
        console.log('找到浮层,', h.bounds().centerX(), ',', h.bounds().centerY())
        click(h.bounds().centerX(), h.bounds().centerY())
        sleep(500)
        click(h.bounds().centerX(), h.bounds().centerY())
    }
    sleep(2000)
    if (currentActivity().indexOf('MainFrame') > 0) {
        click(centerX, centerY)
    }
    sleep(2000)
}

let startCoin = null // 音量键需要

function double11Main() {
    if (autoOpen) {
        openAndInto()
        for (var i=0;i<2;++i) {
            console.log('等待活动页面加载')
            if (!findTextDescMatchesTimeout(/.*开心愿奖.*/, 8000)) {
                console.log('未能进入活动，请重新运行！')
                if (i == 0) {
                    clickText(closePopupRegx, 1000)
                    enterAction()
                    continue
                } else {
                    quit()
                }
            }
            break
        }
        console.log('成功进入活动，向下滑动一段以保证控件全部出现')
        sleep(2000)
        scrollDown();
 
        openTaskList();
    } else {
        alert('请关闭弹窗后立刻手动打开京东App进入活动页面，并打开任务列表', '限时30秒')
        console.log('请手动打开京东App进入活动页面，并打开任务列表')
        if (!findTextDescMatchesTimeout(/累计任务奖励|互动攻略/, 30000)) {
            console.log('未能进入活动，请重新运行！')
            quit()
        }
        console.log('成功进入活动')
    }
 
    sleep(5000)
 
    try {
        console.log('获取初始金币数量')
        startCoin = getCoin()
        console.log('当前共有' + startCoin + '金币')
    } catch (err) {
        console.log('获取金币失败，跳过', err)
    }
 
    sleep(1000)
    havestCoin()
    sleep(1000)
    
    clickText(closePopupRegx, 1000)
 
    // 完成所有任务的循环
    while (true) {
        let [taskButton, taskText, taskCount, taskTitle] = getTaskByText()
 
        if (!taskButton) {
            console.log('领取累计奖励')
            textContains('去领取').find().forEach(function (e, i) {
                console.log('领取第' + (i + 1) + '个累计奖励')
                e.click()
                sleep(2000)
            })
 
            sleep(1000)
            havestCoin()
            sleep(1000)
 
            console.log('最后进行签到任务')
            signTask()
 
            let endCoin = null
            try {
                console.log('获取结束金币数量')
                endCoin = getCoin()
                console.log('当前共有' + endCoin + '金币')
            } catch (err) {
                console.log('获取金币失败，跳过', err)
            }
 
            console.log('没有可自动完成的任务了，退出。')
            console.log('互动任务、下单任务需要手动完成。')
            if (startCoin && endCoin) {
                console.log('本次运行获得' + (endCoin - startCoin) + '金币')
            } else {
                console.log('本次运行获得金币无法计算，具体原因请翻阅日志。')
            }
 
            // alert('任务已完成', '别忘了在脚本主页领取年货节红包！')
            console.log('任务已完成', '互动任务手动完成之后还会有新任务，建议做完互动二次运行脚本')
            break
        }
 
        if (taskText.match(/品牌墙|种草城/)) { // 品牌墙0/3只需要一次完成
            taskCount = 1
        }
 
        // 根据taskCount进行任务，一类任务一起完成，完成后刷新任务列表
        console.log('进行' + taskCount + '次“' + taskText + '”类任务')
        for (let i = 0; i < taskCount; i++) {
            console.log('第' + (i + 1) + '次')
            let taskFlag = doTask(taskButton, taskText, taskTitle)
            if (taskFlag) {
                console.log('完成，进行下一个任务')
            } else {
                console.log('任务失败，尝试重新打开任务列表获取任务')
                break // 直接退出，无需在此调用reopen
            }
        }
        console.log('重新打开任务列表获取任务')
        reopenTaskList()
    }
    if (autoConsume) eatCoin()
}

function closeDialog(txt) {
    let renwu = findTextDescMatchesTimeout(txt, 1000)
    if (!renwu) {
        return false
    }
    let x = device.width - 70, y = renwu.bounds().top - 60
    console.log('关闭弹窗',x,y)
    click(x,y)
    return true
}

function clickAllText(txt, upper, down) {
    let c = 0
    let timeout = 2000
    let top = device.height/2, bottom = device.height/5
    let renwu = findTextDescMatchesTimeout(upper, timeout)
    if (renwu) {
        top = renwu.bounds().bottom
    }
    renwu = findTextDescMatchesTimeout(down, timeout)
    if (renwu) {
        bottom = renwu.bounds().top
    }
    while (c < timeout / 50) {
        let result = textMatches(txt).boundsInside(100, top, 100, bottom).findOnce() || descMatches(txt).boundsInside(100, top, 100, bottom).findOnce()
        if (result) return clickElement(result)
        sleep(50)
        c++
    }
    return false
}

/**
 * 获取状态栏高度
 *
 * @returns
 */
 function getStatusBarHeightCompat () {
    let result = 0
    let resId = context.getResources().getIdentifier("status_bar_height", "dimen", "android")
    if (resId > 0) {
      result = context.getResources().getDimensionPixelOffset(resId)
    }
    if (result <= 0) {
      result = context.getResources().getDimensionPixelOffset(R.dimen.dimen_25dp)
    }
    return result
}
let barHeight = 0
let img = 0
function ocrImgInPath(txt, path) {
    img && img.recycle()
    img = images.read(path)
    if (!img ) {
      toastLog('截图失败')
      return false
    }
    if (!barHeight) {
        barHeight = getStatusBarHeightCompat()
    }
    let result = $ocr.detect(img)
    for (let i =0; i < result.length; ++i) {
        let or = result[i]
        if (or.label.match(txt)) {
            let x = (or.bounds.left + or.bounds.right)/2
            let y = (or.bounds.top+or.bounds.bottom)/2 - barHeight 
            console.log(or.label, x, y)
            return click(x, y)
        }
    }
    return false
}
  

function eatCoin() {
    backToMain()
    enterAction()
    while (1) {
        if (!clickText(/消耗/)) {
            break
        }
        sleep(8000)
        // clickAllText(/开心收下/, /.*优惠券.*|/)
        if (!captureAndOcr(/开心收下|去鉴宝/)) {
            return
        }
        closeDialog(/完成任务可.*/)        
        let renwu = findTextDescMatchesTimeout(/.*做任务.*/, 2000)
        if (renwu) {
            console.log('金币不足' )
            break
        }
    }
}

function backToMain(activity, pkg) {
    scrollUp()
    return backToText(/首页/)
    if (!activity) {
        activity = 'MainFrame'
    }
    openApp(pkg)
    for (let i = 0; i < 10; i++) { // 尝试返回3次
        if (currentActivity().indexOf(activity) >= 0) {
            break
        }
        console.log('非主页，继续返回')
        back()
        sleep(1000)
    }    
    scrollUp()
}

function backToText(text) {
    let timeout = 2000
    let ret = false
    for (let i=0;i<5; ++i) {
        let renwu = findTextDescMatchesTimeout(text, timeout)
        if (!renwu) {
            console.log('无法找到', text, '返回')
            back()
        } else {
            ret = true
            break
        }
    }
    return ret
}

function getTasksByText(txt, ignores, textFinder, white) {
    let tButton = null,
        tText = '',
        tCount = 0,
        tTitle = ''
    console.log('寻找未完成任务...')
    let taskButtons = textMatches(txt).find()
    if (!taskButtons.empty()) { // 如果找不到任务，直接返回
        for (let i = 0; i < taskButtons.length; i++) {
            let button = taskButtons[i]
            let lst = []
            if (textFinder) {
                textFinder(button, lst)
            } else {
                let tmp = button.parent().child(button.indexInParent() - 1)
                tTitle = tmp.child(0).text()
                tText = tmp.child(1).text()
                lst.push(tTitle, tText)
            }
            if (lst.length == 0) continue
            tTitle = lst[0]
            if (lst.length > 1) tText = lst[1]

            let r = tTitle.match(/(\d*)\/(\d*)/)
            if (r) {
                tCount = (r[2] - r[1])
            } else {
                tCount = 0
            }

            let find = false
            if (ignores) {
                for (let ndx = 0; ndx < ignores.length; ++ndx) {
                    find = tText.match(ignores[ndx]) || tTitle.match(ignores[ndx])
                    if (find) {
                        find = true
                        break
                    }
                    find = false
                }
            }
            if (white && !find) {
                for (let ndx = 0; ndx < white.length; ++ndx) {
                    r = tText.match(white[ndx]) || tTitle.match(white[ndx])
                    if (!r) {
                        find = true
                    }
                }
            }
            if (!find && taskCount[tTitle] > 11) {
                find = true
            }
            // if (!find && !validBounds(button)) {
            //     find = true
            // }
            console.log('任务:', button.text(), tTitle, tCount, find)
            if (find) continue 
            tButton = button
            break
        }
    } else {
        console.log('任务提示未找到')
    }
    if (tButton)
        taskCount[tTitle] = ((taskCount[tTitle]||0) + 1)
    return [tButton, tText, tCount, tTitle]
}

function validBounds(ele) {
    if (ele.bounds().centerX() <= 0 || ele.bounds().centerX() >= device.width) {
        return false
    }
    if (ele.bounds().centerY() <= 0 || ele.bounds().centerY() >= device.height) {
        return false
    }
    return true
}

function clickElement(ele) {
    let a=ele.click()
    sleep(500)
    if (a != ele.click()) console.log("ele1")
    if (validBounds(ele)||1) {
        click(ele.bounds().centerX(), ele.bounds().centerY())
        return true
    }
    return a
}

function clickText(txt, timeoutOrAfterTxt, boundType, addon) {
    let tp = typeof timeoutOrAfterTxt
    let timeout = 10000
    let afterTimeout = 3000
    let afterTxt = null
    if (tp == 'number') {
        timeout = timeoutOrAfterTxt
    } else if ((timeoutOrAfterTxt instanceof Array)) {
        timeout = timeoutOrAfterTxt[0] || timeout
        afterTxt = timeoutOrAfterTxt[1] || afterTxt
        afterTimeout = timeoutOrAfterTxt[2] || afterTimeout
    } else {
        afterTxt = timeoutOrAfterTxt
    }
    // console.log(txt, timeout, afterTimeout, afterTxt)
    let renwu = findTextDescMatchesTimeout(txt, timeout)
    if (!renwu) {
        console.log('无法找到', txt)
        return false
    }
    let x=0,y=0
    let r = false
    if (!boundType) {
        console.log('点击', txt)
        r = clickElement(renwu)
    } else if (boundType == 1) {
        x=renwu.bounds().left
        y=renwu.bounds().top
    } else if (boundType == 2) {
        x=renwu.bounds().centerX()
        y=renwu.bounds().centerY()
    } else if (boundType == 3) {
        x = renwu.bounds().right
        y = renwu.bounds().bottom
    }     
    if (addon && addon.length > 1) {
        x = x + addon[0]
        y = y + addon[1]
    }
    if (x && y) {
        console.log('点击', txt, x, y)
        r = click(x, y)
    }
    if (afterTxt) {
        renwu = findTextDescMatchesTimeout(afterTxt, afterTimeout)
    }
    sleep(afterTimeout)
    return r
}

function FreeFruit() {
    this.qiandao = 0
    this.getwater = 0
    this.friends = 0
    this.friendCount = 0
    this.nameQianDao = /8ff8554110b01c71.*|签到/
    this.nameGetWater = /f948ff17c79c25fa.*|领水滴/
    this.nameFriend = /228c5c13eaf85e98.*|好友/
    this.nameWaterOut = /86b551d1155595c3.*|浇水/

    this.taskQiandao = function() {
        if (this.qiandao) {
            return
        }
        if (!clickText(this.nameQianDao, /.*礼包/)) 
            return false
        clickText(/签到领.*|领取.*礼包/, 1000)
        clickText(/明日.*/, 1000)
        for (let ndx=0;ndx<3;++ndx) {
            this.closePopup()
            if (clickText(/关注得水滴/, 1000)) {
                sleep(5500)
                backToText(/东东农场/)
                clickText(/去领取/)
                continue
            }
            break
        }
        backToText(this.nameQianDao)
        this.qiandao = 1
    }
    this.textFinder = function(button, lst) {
        let p = button.parent()
        p.parent().children().forEach(function(child) {
            if (child.text()) {
                if (lst.length < 2) {
                    lst.push(child.text())
                } else {
                    lst[1] = lst[1]+child.text()
                }
            }
        })
    }
    this.textFinderFriend = function(button, lst) {
        button = button.parent().parent()
        if (button.indexInParent() > 2) {
            lst.push(button.parent().child(button.indexInParent()-2).text())
            lst.push(button.parent().child(button.indexInParent()-1).text())
        } else {
            let find = false
            button.parent().children().forEach(item => {
                if (find) {
                    return false
                }
                let rect1 = item.bounds()
                let rect2 = button.bounds()
                find = sameRect(rect1, rect2)
                if (find) {
                    return false
                }
                lst.push(item.text())
                return true
            })
            if (find) {
                while (lst.length > 2) {
                    lst.shift()
                }
            }
        }
    }   
    this.gotoJindou = function() {
        clickText(/领京豆/, /再升级.*/)
        backToMain()
        clickText(/免费水果/, /再浇.*/)
        clickText(this.nameGetWater, /领水滴/)
    }
    this.closeDialog = function(txt) {
        let renwu = findTextDescMatchesTimeout(txt, 1000)
        if (!renwu) {
            return false
        }
        renwu = renwu.parent()
        let p = renwu.parent()
        if (p.childCount() > 2) {
            console.log('关闭弹窗')
            clickElement(p.child(p.childCount()-1))
            return true
        }
        let x = device.width - 40, y = renwu.bounds().centerY()
        console.log('关闭弹窗',x,y, renwu)
        click(x,y)
        return true
    }
    this.taskWater = function() {
        if (this.getwater || !clickText(this.nameGetWater, /领水滴/)) {
            return
        }
        lst = [/下单|水滴雨|万物商店|万人/]
        while (1) {
            this.closePopup()
            let [taskButton, taskText, taskCount, taskTitle] = 
                getTasksByText(/去完成|领取|去领取|去逛逛/, lst, this.textFinder)
            if (!taskButton) {
                break
            }
            if (taskTitle.indexOf('好友浇水') > 0 && taskButton.text().indexOf('领取') < 0) {
                this.friendCount = 2
                lst.push(/好友/)
                continue
            }
            clickElement(taskButton)
            if (taskButton.text().indexOf('领取') >= 0) {
                sleep(2000)                
            } else if (taskTitle.indexOf('免费水果') > 0) {
                sleep(5500) // 从首页重入
                return
            } else if (taskTitle.indexOf('领京豆') > 0) {
                this.gotoJindou()
                sleep(2000)
            } else if (taskText.indexOf('浏览') >= 0) {
                r = taskText.match(/(\d+)[秒s]/)
                timeout = 2500
                if (r) {
                    timeout = parseInt(r[1]) * 1000 + 500
                }
                sleep(timeout)
            } else {
                console.log(taskTitle, '重开任务列表')
            }
            backToText(/东东农场/)
            this.closeDialog(/领水滴/)
            sleep(1000)
            clickText(this.nameGetWater, /领水滴/)
            sleep(1000)
        }
        if (!this.closeDialog(/领水滴/)) {
            console.log('没有找到任务列表的关闭按钮')
        }
        sleep(1000)
        this.getwater = 1
    }
    this.taskFriend = function() {
        if (this.friends) {
            return
        }
        if (!clickText(this.nameFriend, /好\s*友/)) {
            this.friends = 1
            return
        }
        if (findTextDescMatchesTimeout(/.*还没有好友.*/,2000)) {
            this.friends = 1
            if (!this.closeDialog(/好\s友/)) {
                console.log('没有找到好友列表的关闭按钮')
            }
            return
        }
        let count = 1
        let white = [/豆豆/,/jd_WjohMbSkSEhV/]
        while (count && this.friendCount) {
            this.closePopup()
            let [taskButton, taskText, taskCount, taskTitle] = 
                getTasksByText(/26e11fbe52815a36.*|浇水/, [/邀请/], this.textFinderFriend, white)
            if (!taskButton) {
                if (white.length > 0) {
                    white = []
                    continue
                }
                break
            }
            clickElement(taskButton)
            sleep(2000)
            clickText(this.nameWaterOut, /好\s*友/)
            backToText(/好\s*友/)
            --this.friendCount
        }
        if (!this.closeDialog(/好\s友/)) {
            console.log('没有找到好友列表的关闭按钮')
        }
        sleep(1000)
        this.friends = 1
    }
    this.closePopup = function () {
        for (let i=0;i<3;++i) {
            if (clickText(/领取水滴|收下水滴|去签到|去领取|回农场.*/, 2000)) {
                backToText(/东东农场/)
            } else {
                break
            }
        }
    }
    this.main = function() {
        if (getStorageTimeSub(account+'.fruit') < 1000*3600) {
            return
        }
        openAndInto(/免费水果/)
        if (clickText(/.*太火爆.*|再浇.*/, 8000)&&clickText(/.*太火爆.*/, 1000)) {
            backToMain()
            return
        }
        this.closePopup()
        let count = 0
        while ((!this.qiandao || !this.getwater || ! this.friends) && count < 5) {
            count++
            if (!findTextDescMatchesTimeout(/东东农场/, 5000)) {
                openAndInto(/免费水果/)
                sleep(2000)
                continue
            }
            this.taskQiandao()
            this.taskWater()
            this.taskFriend()
        }
        if (clickText(/连续点.*/)) {
            sleep(4500)
        }
        clickText(this.nameWaterOut) // 浇水
        console.log('免费水果结束')
        backToMain()
        storageOp(account+'.fruit', 1, (new Date()).getTime())
    }
}

function JingDou() {
    this.tasks = 0
    this.textFinder = function(button, lst) {
        let p = button.parent()
        p.parent().children().forEach(function(child) {
            if (child.text()) lst.push(child.text())
        })
    }
    this.gotoFruit = function() {
        clickText(/免费水果/, /东东农场/)
        sleep(2000)
        backToMain()
        clickText(/领京豆/, /再升级.*/)
    }
    this.taskAction = function() {
        if (this.tasks) {
            return
        }
        clickText(/再升级得.*/, /.*可额外得.*/, 3, [10, 10])
        while (1) {
            let [taskButton, taskText, taskCount, taskTitle] = getTasksByText(/去完成/, [/下单|收集水滴|浇水/], this.textFinder)
            if (!taskButton) {
                let renwu = findTextDescMatchesTimeout(/做任务.*/, 1000)
                if (!renwu) {
                    backToMain()

                }
                break
            }
            clickElement(taskButton)
            if (taskTitle.match(/.*免费水果.*/)) {
                this.gotoFruit()
                sleep(1000)
                clickText(/再升级得.*/, /.*可额外得.*/, 3, [10, 10])
            } else if (taskText.indexOf('浏览') >= 0) {
                r = taskText.match(/(\d+)[秒s]/)
                timeout = 6500
                if (r) {
                    timeout = parseInt(r[1]) * 1000 + 500
                }
                sleep(timeout)
                continue
            }
            else {
                sleep(1500)
            }
            backToText(/做任务.*/)
        }
        this.tasks = 1
    }
    this.种豆textFinder = function(button, lst) {
        function nodeWalker(node) {
            if (node.text()) {
                lst.push(node.text())
                return
            }
            node.children().forEach(nodeWalker)
        }
        button.parent().child(0).children().forEach(nodeWalker)
        if (lst.length > 1) {
            button = lst.shift()
            let tmp = lst.join('')
            while (lst.length) {
                lst.shift()
            }
            lst.push(button)
            lst.push(tmp)
        }
    }
    this.关闭种豆任务列表 = function () {
        let loop = 3
        while (loop--) {
            let renwu = findTextDescMatchesTimeout(/完成任务越多.*/, 2000)
            if (!renwu) {
                console.log('没有找到/完成任务越多.*/')
                return loop != 2
            }
            let cnt = renwu.parent().childCount()
            renwu = renwu.parent().child(cnt-1)
            if (clickElement(renwu)) break
            sleep(2000)
        }
    }
    this.种豆得豆 = function (loop) {
        if (loop && loop > 3) return false
        loop = loop || 0
        {
            let renwu = findTextDescMatchesTimeout(/再升级得.*/, 1000)
            if (!renwu) {
                return false
            }
            let x = device.width - 90, y = renwu.bounds().bottom
            console.log('种豆得豆',x,y)
            click(x,y)
        }
        if (!clickText(/种豆得豆/, /更多任务/)) {
            return false
        }
        clickText(/更多任务/, [3000, /.*任务越多.*/, 4000])
        swipe(device.width / 2, device.height - 200, device.width / 2 + 20, device.height - 500, 1000)
        sleep(2000)
        swipe(device.width / 2 + 20, device.height - 500, device.width / 2, device.height - 200, 1000)
        sleep(3500)
        let cnt = 1
        while (cnt) {
            let [taskButton, taskText, taskCount, taskTitle] = getTasksByText(/去逛逛/, [], this.种豆textFinder)
            if (!taskButton) {
                let renwu = findTextDescMatchesTimeout(/.*任务越多.*/, 1000)
                if (!renwu) {
                    openAndInto(/领京豆/)
                    return this.种豆得豆(loop+1)
                }
                break
            }
            //console.log(taskButton.text(), taskTitle, taskText, taskButton.bounds().centerX(), taskButton.bounds().centerY())
            if (!clickElement(taskButton)){
                sleep(1000)
                console.log("点击失败")
            }
            if (taskTitle.indexOf('免费水果') > -1) {
                if (clickText(/去完成/, 2000)) {
                    this.gotoFruit()
                    sleep(1000)
                    this.种豆得豆(loop+1)
                }
            }
            let timeout = 2500
            sleep(timeout)
            if(!validBounds(taskButton)||clickText(/.*任务越多.*/)) {
                swipe(device.width / 2, device.height - 200, device.width / 2 + 20, device.height - 500, 1000)
                sleep(2000)
                continue
            }
            backToText(/.*瓜分.*/)
            if(!clickText(/.*任务越多.*/))
                clickText(/更多任务/, /.*任务越多.*/)
        }
        this.关闭种豆任务列表()
        cnt = 10
        while (clickText(/x[1-9]\d*/, 2000) && cnt--) {}
        backToText(/种豆得豆/)
        return true
    }
    this.main = function() {
        if (getStorageTimeSub(account+'.jingdou') < 1000*3600) {
            return
        }
        openAndInto(/领京豆/)
        this.种豆得豆()
        backToText(/已连签.*/)
        let count = 0
        while ((!this.tasks) && count < 5) {
            count++
            if (!findTextDescMatchesTimeout(/已连签.*/, 5000)) {
                openAndInto(/领京豆/)
                sleep(2000)
                continue
            }
            this.taskAction()
        }
        console.log('领京豆结束')
        backToMain()
        storageOp(account+'.jingdou', 1, (new Date()).getTime())
    }
}

function getTimeout(txt) {
    r = txt.match(/(\d+)[秒s]/)
    let timeout = 6500
    if (r) {
        timeout = parseInt(r[1]) * 1000 + 500
    }
    return timeout
}

function getCount(txt) {    
    r = txt.match(/(\d+)\/(\d+)/)
    let cnt = 1
    if (r) {
        cnt = parseInt(r[2]) - parseInt(r[1])
    }
    return cnt
}

function LingQuan() {
    this.textFinder = function (button, lst) {        
        let tmp = button.parent().child(button.indexInParent() - 2)
        lst.push(tmp.text())
        lst.push('')
    }
    this.enter = function(timeout) {
        backToMain()
        clickText(/领券/, /领券中心/)
        showConsole(0)
        if (clickText(/立即领取/, 1000)) {
            backToText(/领券中心/)
        }
        if(clickText(/.*签到.*/, /签到提醒/)) {
            clickText(closePopupRegx, 1000)
            backToText(/.*中心/, undefined, undefined, 2000)
        } 
        sleep(timeout || 1000)
        clickText(/返回/, /每日.*/, 2, [80, 0])
        if (clickText(/.*待领取.*|天天攒.*/)) {
            swipe(device.width / 2, device.height - 200, device.width / 2 + 20, device.height - 500, 1000)
        }
        showConsole(1)
    }
    this.collect = function() {
        scrollUp()
        scrollUp()
        scrollUp()
        scrollUp()
        console.log('开始收集')
        let regx = /\+\d+.+/
        let rect = findTextDescMatchesTimeout(/规则/, 1000)
        let i = 15
        // if (rect) {
        //     rect = rect.parent().bounds()
        //     let result = textMatches(regx).boundsInside(rect.left, rect.top, rect.right, rect.bottom).findOnce() ||
        //         descMatches(regx).boundsInside(rect.left, rect.top, rect.right, rect.bottom).findOnce()
        //     while (result && i) {
        //         clickElement(result)
        //         sleep(1000)
        //         result = textMatches(regx).boundsInside(rect.left, rect.top, rect.right, rect.bottom).findOnce() ||
        //         descMatches(regx).boundsInside(rect.left, rect.top, rect.right, rect.bottom).findOnce()
        //         --i
        //     }
        //     return
        // }
        while (clickText(regx) && i) {
            sleep(1000)
            --i
            clickText(/.*立即领取/, 1000)
        }
    }
    this.main = function() {
        if (getStorageTimeSub(account+'.diandianquan') < 1000*3600) {
            return
        }
        this.enter()
        if (!findTextDescMatchesTimeout(/天天点点券/, 3000) ) {
            console.log('未进入点点券界面')
            return
        } 
        let loop = 3       
        while (loop) {
            let [taskButton, taskText, taskCount, taskTitle] = getTasksByText(/领取任务|继续完成/, [], this.textFinder)
            if (!taskButton) {
                if (loop == 3) {
                    backToText(/天天点点券/)
                    this.collect()
                }
                --loop
                sleep(2000)
                continue
            }
            loop = 3
            let timeout = getTimeout(taskTitle)
            clickElement(taskButton)
            sleep(1000)
            if (taskTitle.indexOf('去点击“领券”') > 0) {
                console.log('重新进入')
                sleep(5000)
                this.enter(15500)
                continue
            }
            let ele = idMatches(/all-task-popup/).findOnce()
            if (ele) {
                let close = ele.child(0).child(0)
                ele = ele.child(0).child(1)
                let text = ele.text()
                console.log(text, ele.bounds())
                timeout = getTimeout(text)
                let cnt = getCount(text)
                for (let i=0;i<cnt;++i) {
                    clickElement(ele.parent().child(ele.indexInParent() + 1).child(0))
                    sleep(2000)
                    clickText(/继续浏览/, timeout>2000?timeout-2000:timeout)
                    backToText(/天天点点券/) 
                    sleep(2000)
                }
                console.log('关闭弹窗')
                clickElement(close) 
                sleep(1000)
                backToText(/天天点点券/) 
                continue
            }
            sleep(timeout)
            backToText(/天天点点券/)
            scrollUp()            
            if (findTextDescMatchesTimeout(/我的集卡.*/, 1000) ) {
                this.enter()
            } 
        }
        let tm = new Date()
        storageOp(account+'.diandianquan', 1, tm.getTime())
    }
}

function sameRect(rect1, rect2) {
  return (rect1.top == rect2.top && 
    rect1.left == rect2.left && 
    rect2.right == rect1.right && 
    rect1.bottom == rect2.bottom)
}

function getIndexInParent(button) {
    if (button.indexInParent() > -1) {
        return button.indexInParent()
    }
    for(let i = 0; i < button.parent().childCount(); i++){
        let child = button.parent().child(i);
        let rect1 = item.bounds()
        let rect2 = button.bounds()
        if (sameRect(rect1, rect2)) {
            return i
        }
    }
    return null
}

function MiaoMiaoBi() {
    this.textFinder = function (button, lst) {
        let index = getIndexInParent(button)
        let tmp = button.parent().child(index - 2)
        lst.push(tmp.text())
        tmp = button.parent().child(index - 1)
        lst.push(tmp.text())
    }
    this.qiandao = function() {
        let button = findTextDescMatchesTimeout(/签到|sign-in-redpack-and-coin.*/, 1000)
        if (!button) {
            console.log('没找到签到')
            return
        }
        button.parent().parent().children().forEach(item => {
            if (item.childCount() == 3 && !item.child(1).text().match(/第.天/)) {
                clickElement(item.child(1))
            }
        })
        sleep(2000)
    }
    this.main = function () {
        backToMain()
        clickText(/生活.*缴费/, 2000, 2, [0, 130])
        showConsole(0)
        if (!clickText(/签到/, 3000)) {
            clickText(/返回/, 2000, 3, [0, 80])
        }
        showConsole(1)
        clickText(/天天领红包/, 3000)
        let loop = 3       
        while (loop) {
            let [taskButton, taskText, taskCount, taskTitle] = getTasksByText(/去完成/, [], this.textFinder) 
            if (!taskButton) {
                break
            }
            clickElement(taskButton)
            r = taskText.match(/(\d+)[秒s]/) || taskTitle.match(/(\d+)[秒s]/)
            let timeout = 2500
            if (r) {
                timeout = parseInt(r[1]) * 1000 + 500
            }
            sleep(timeout)
            backToText(/天天领红包/)
        }
        this.qiandao()
        backToMain()
        sleep(1500)
    }
}

function aliPay() {
    this.蚂蚁庄园 = 0
    this.蚂蚁新村 = 0
    this.蚂蚁森林 = 0
    this.芭芭农场 = 0
    this.main = function() {
        openAndInto(/蚂蚁庄园/, 'com.eg.android.AlipayGphone')
        while (!this.蚂蚁庄园 || !this.蚂蚁新村 || ! this.蚂蚁森林 || ! this.芭芭农场) {
        }
    }
}

function storageOp(key, put, val) {
    if (put) {
        return storage.put(key, val)
    } 
    return storage.get(key, val)
}

function getStorageTimeSub(key) {
    let tm = new Date()
    let tm2 = parseInt(storage.get(key, 0))
    return (tm.getTime() - tm2)
}

function swipeLeftOrRight(left) {
    if (left) {
        swipe(device.width-200, device.height/2-100, 200, device.height/2, 1000)
    } else {
        swipe(200, device.height/2, device.width-200, device.height/2-100, 1000)
    }
    sleep(2000)
}

function switchAccount() {
    // let appName = '超级多开',pkg = 'com.polestar.super.clone'
    let appName = 'BlackBox64',pkg = 'top.niunaijun.blackboxa64'
    g_pkgName = pkg 
    if (!app.getAppName(pkg)) {
      appName = 'BlackBox32'
      g_pkgName = app.getPackageName(appName)
    }
    for (let i=0;i<4;++i) {
        let name = '/京东\\s*' + (i+1) +'*/' 
        account = name
        if (getStorageTimeSub(account) < (1000*3600)) {
            console.log(account, '最近跑过，跳过')
            continue
        }
        clearRecentApp()
        startAppInDesktop(eval('/'+appName+'/'))
        if (!findTextDescMatchesTimeout(/京东.*/, 8000)) {    
            app.launchApp(appName)
            console.log('多开界面打开失败')
            return false
        }
        for (let j=0;j<i;++j) {
            swipeLeftOrRight(1)
            sleep(1000)
        }
        if (clickText(eval('/User ' + i + '/'))) {
            clickText(/取消/)
        } else {
            continue
        }
        showConsole(0)
        let ret = clickText(eval(name))
        clickText(eval(name))
        showConsole(1)
        if (!ret) {
            console.log('退出切换') 
            return false
        }
        if (findTextDescMatchesTimeout(/.*京豆.*/, 20000)) {
            console.log('打开了'+name)
            clickText(/我的/)
            sleep(3000)
            clickText(/首页/)
            clickText(/首页/)
            jdMain()
        } else {
            console.log(name,'打开失败')
        }
    }
}

function startAppInDesktop(name) {
    let left = true;
    swipeLeftOrRight(left)
    sleep(1000)
    for (let i=0;i<2;++i) {
        let ret = findTextDescMatchesTimeout(name, 2000)
        if (ret) {
            console.log("从桌面启动",ret.bounds().centerX(), ret.bounds().centerY() - 260)
            click(ret.bounds().centerX(), ret.bounds().centerY() - 260)
            click(ret.bounds().centerX(), ret.bounds().centerY() - 260)
            sleep(3000)
            clickElement(ret)
            sleep(3000)
            break
        }
    }
}

function screenShotInNeight() {
    let dt = new Date()
    if (dt.getHours() > 7) {
        return
    }
    sysScreenShot(false)
}

function sysScreenShot(hideConsole) {
    let f1 = [0, 1000, [device.width/2, 200], [device.width/2, 1600]]
    let f2 = [20, 1000, [device.width/2-150, 230], [device.width/2-150, 1630]]
    let f3 = [20, 1000, [device.width/2+150, 230], [device.width/2+150, 1630]]
    hideConsole? showConsole(0) : false
    gestures(f2, f1, f3)
    sleep(1000)
    hideConsole? showConsole(1) : false
    let path = files.join(files.cwd(), '/../DCIM/Screenshots')
    let lst = files.listDir(path, name => {
        return name.endsWith(".jpg") && files.isFile(files.join(path, name))
    })
    lst.sort()
    return files.join(path,lst[lst.length-1])
}

function clearRecentApp(all) {
    if (all == undefined || all) home()
    recents()
    sleep(2000)
    clickText(/本机共有.*/)
    sleep(1500)
    back()
    sleep(1500)
}

function dblBackQuitApp() {
    sleep(2000)
    backToMain()
    back()
    sleep(200)
    back()
    console.log("已退出")
    sleep(1000)
}

function jdMain() {
    let ret = false
    try {
        if (getStorageTimeSub(account) < (1000*3600)) {
            console.log(account, '最近跑过，跳过')
            return ret
        }
        //double11Main() 
        //clearRecentApp(false)
        backToMain()
        let obj = new FreeFruit()
        obj.main()
        clearRecentApp(false)
        obj = new JingDou()
        obj.main()
        clearRecentApp(false)
        obj = new LingQuan()
        for (let i=0;i<3;++i) {
            obj.main()
        }
        clearRecentApp(false)
        obj = new MiaoMiaoBi()
        obj.main()
        ret = true
        
        tm = new Date()
        storage.put(account, tm.getTime())
    } catch (err) {
        console.error(err)
    }
    // startAppInDesktop()
    dblBackQuitApp()
    return ret
}

function showConsole(show) {
    if (show) {
        console.show()
        sleep(500)
        console.setSize(device.width-100, 400)
    } else {
        console.hide()
    }

}

function openRustDesk() {
    if (device.model != 'Redmi Note 4X') {
        return
    }
    pkg = 'com.carriez.flutter_hbb'
    openApp(pkg, /.*Rust.*/)
    clickText(/.*共享[\S\s]*/, 5000)
    if (!clickText(/启动服务/, 2000)) {
        return
    }
    clickText(/确认/, 2000)
    clickText(/立即开始.*/, 2000)
}

function 充电() {
    if (device.model != 'Redmi Note 4X') {
        return
    }
    let battery = device.getBattery()
    let isCharging = device.isCharging()
    console.log('充电', battery, isCharging) 
    let act = ''
    if (isCharging) {
        if (battery > 90) {
            act = 'off'
        }
    } else {
        if (battery <= 25) {
            act = 'on'
        }
    }
    if (!act) return
    pkg = 'com.kankunit.smartplugcronus'
    openApp(pkg, /智能插座/)
    let i = 3
    while (i--) {
        clickText(/智能插座/, /定时任务/)
        if (clickText(/timer_mg/)) break
        let ele = id('device_btn').findOnce()
        if (ele) {
            console.log('点击 device_btn')
            clickElement(ele)
            break
        }
    }
}

function findClose() {
    sleep(4000)
    img && img.recycle()
    img = images.load('https://gitee.com/makeavailable/VideoAudioManager/raw/master/auto_gui/autojs/resources/close1.png')
    let path = sysScreenShot(true)
    let img2 = images.read(path)
    let ret = images.findImage(img2, img, { threshold:0.1})
    if (!barHeight) {
        barHeight = getStatusBarHeightCompat()
    }
    if (ret) {
        ret.y = ret.y + 50
        ret.x = ret.x + 50
        click(ret.x, ret.y)
    }
    console.log(ret, path, img.getWidth())
    img2.recycle()
    img.recycle()
    img = 0
    files.remove(path) 
}

function test() {    
    let appName = 'BlackBox64',pkg = 'top.niunaijun.blackboxa64'
    console.log(app.getPackageName(appName), app.getAppName(pkg))
    quit()
}
 
let account = 'alone'
// 全局try catch，应对无法显示报错 
try {
    充电()
    openRustDesk()
    openAndInto(/首页/)
    // test()
    jdMain()
    switchAccount()
    console.log('所有任务完成', '脚本结束运行！') 
    clearRecentApp()
    showConsole(0)
    quit()
} catch (err) {
    device.cancelKeepingAwake()
    if (err.toString() != 'JavaException: com.stardust.autojs.runtime.exception.ScriptInterruptedException: null') {
        console.error(err)
        startCoin && console.log('本次任务开始时有' + startCoin + '金币')
    }
}