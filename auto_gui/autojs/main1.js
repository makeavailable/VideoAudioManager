

function getEntries (regx) {
    let countDown = new java.util.concurrent.CountDownLatch(1)
    let entryIcon = null
    threads.start(function () {
      entryIcon = selector().textMatches(regx).untilFind()
      countDown.countDown()
    })
    countDown.await(5, java.util.concurrent.TimeUnit.SECONDS)
    return entryIcon
}

let plantBean = idMatches(/.*?title_right_opt.*/).find()
console.log(plantBean.length)