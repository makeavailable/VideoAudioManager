
function askKimi() {
    let headers = {
        authorization: '',
        "content-type": 'application/json',
        referer: 'https://kimi.moonshot.cn/',
        cookie: '',
        url: ''
    }
    let confPath = files.cwd() + '/' + 'updateConfig.json'
    if (files.exists(confPath)) {
        let str = files.read(confPath)
        headers = JSON.parse(str)
    } else {
        files.write(confPath, JSON.stringify(headers, null, 2))
        return
    }
    let options = {
        headers: headers
    }

    // 分配地址
    let url = 'https://kimi.moonshot.cn/api/pre-sign-url'
    let path = sysScreenShot()
    data = {
      "name": files.getName(path),
      "action": "image"
    }
    console.log(data)
    let r = http.postJson(url, data, options)
    data = r.body.string()
    console.log(data)
    if(r.statusCode != 200){
        toast("请求失败: " + url);
        return
    }
    let s3Obj = JSON.parse(data)

    // 上传文件
    url = s3Obj.url
    options = {
        headers: {referer:headers.referer},
        // files: { file: open(path) },
        method: 'PUT',
        contentType: "application/x-www-form-urlencoded",
        body: (sink) => {
            sink.write(files.readBytes(path))
        }
    }
    r = http.request(url, options)
    data = r.body.string()
    if(r.statusCode != 200){
        toast("上传文件失败: ")
        console.log(data, r.statusCode, url)
    }
    
    // 校验文件上传
    options = {
        headers: headers
    }
    let img = images.read(path)
    data = {
        "name": files.getName(path),
        "object_name": s3Obj['object_name'],
        "type": "image",
        "file_id": s3Obj['file_id'],
        "meta": {
            "width": `${img.getWidth()}`,
            "height": `${img.getHeight()}`
        }
    }
    img.recycle()
    // console.log(data)
    url = 'https://kimi.moonshot.cn/api/file'
    let rsp = http.postJson(url, data, options)
    files.remove(path)
    console.log(rsp.statusCode, data)

    // 对文件进行推理
    url = headers.url
    data = {
        "kimiplus_id": "kimi",
        "extend": {
            "sidebar": true
        },
        "model": "kimi",
        "use_research": false,
        "use_search": true,
        "messages": [
            {
                "role": "user",
                "content": "图片中是否出现了弹出窗口，所有关闭按钮的位置是什么(给出相对于图片左上角的位置百分比)"
            }
        ],
        "refs": [
            s3Obj['file_id']
        ],
        "history": [],
        "scene_labels": []
    }
    r = http.postJson(url, data, options)
    data = r.body.string()
    //console.log(data)
    if(r.statusCode != 200){
        toast("对话失败 ");
        return
    }
    let txt = ''
    for (let s of data.split('\n')) {
        if (!s.includes(':')) continue
        s = s.slice(s.indexOf(':')+1)
        //console.log(s)
        let obj = JSON.parse(s)
        if (!obj || !obj.event || obj.event !== 'cmpl') {
            continue
        }
        txt += obj.text
    }
    console.log(txt)
    toast("结束 ");
}

function sysScreenShot() {
    let f1 = [0, 1000, [device.width/2, 200], [device.width/2, 1600]]
    let f2 = [20, 1000, [device.width/2-150, 230], [device.width/2-150, 1630]]
    let f3 = [20, 1000, [device.width/2+150, 230], [device.width/2+150, 1630]]
    gestures(f2, f1, f3)
    sleep(1000)
    let path = files.join(files.cwd(), '/../DCIM/Screenshots')
    let lst = files.listDir(path, name => {
        return name.endsWith(".jpg") && files.isFile(files.join(path, name))
    })
    lst.sort()
    return files.join(path,lst[lst.length-1])
}

module.exports = askKimi
