
module.exports = {
  // 配置自定义的签到任务定义信息
  supported_signs: [
    {
      name: '移动云盘签到',
      taskCode: 'ChinamobilePan',
      script: 'ChinamobilePan.js',
      enabled: true,
      subTasks: [
      {
        taskCode: 'ChinamobilePan',  // 定义子任务编号
        taskName: '移动云盘签到',     // 子任务名称
        enabled: true,           // 是否启用子任务
      },
      {
        taskCode: 'aliPan', 
        taskName: '阿里云盘签到',
        enabled: true,
      },
    ]
    }
  ]
}
