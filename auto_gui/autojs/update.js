
function updateFile() {

    let fileInfo = {
        'jdmain.js': 'jdmain.js', 
        'utils.js': 'utils.js', 
        'update.js': 'update.js', 
        'resources/CommonConfigs.js': 'vue_configs/js/components/configuration/CommonConfigs.js',
        'resources/获取当前页面的布局信息.js': '独立工具/获取当前页面的布局信息.js',
        'resources/config.js': 'config.js',
        'resources/main.js': 'main.js'
    }
    if (engines.myEngine().getSource().toString().indexOf('Unify-Sign') >= 0) {
        fileInfo = {
            'core/ChinamobilePan.js': 'core/ChinamobilePan.js', 
            'core/BBFarm.js': 'core/BBFarm.js', 
            'core/BaseSignRunner.js': 'core/BaseSignRunner.js', 
            'core/JingDongBeans.js': 'core/JingDongBeans.js', 
            'core/Taobao-Coin.js': 'core/Taobao-Coin.js', 
            'core/AlipayMerchantCredits.js': 'core/AlipayMerchantCredits.js', 
            'lib/BaseWidgetUtils.js': 'lib/BaseWidgetUtils.js', 
            'update.js': 'update.js', 
            'utils.js': 'utils.js', 
            'extends/CustomConfig.js': 'extends/CustomConfig.js',
            'extends/CustomSignConfig.js': 'extends/CustomSignConfig.js'
        }
    }

    for (let key in fileInfo) {
        let url = "https://gitee.com/makeavailable/VideoAudioManager/raw/master/auto_gui/autojs/" + key
        let path = files.cwd() + '/' + fileInfo[key]
        let r = http.get(url);
        files.write(path, r.body.string())
    }
}

function checkUpdateFile() {
    let myconfig = {
        updateFile:'18:00',
    }
    let confPath = files.cwd() + '/' + 'updateConfig.json'
    if (files.exists(confPath)) {
        let str = files.read(path)
        myconfig = JSON.parse(str)
    }
    let lst = myconfig.updateFile.split(':')
    let dt = new Date()
    dt.setHours(Number.parseInt(lst[0]))
    dt.setMinutes(Number.parseInt(lst[1]))
    let sub = Math.abs(new Date() - dt)
    console.log('updateFile sub time', sub/1000/60)
    if (sub < 10*60*1000) {
        console.clear()
        updateFile()
    }
}
function turnOnOffPower() {
    let url = ''
    console.log('turnOnOffPower', device.getBattery(), device.isCharging())
    if (device.getBattery() > 90 && device.isCharging()) {
        url = 'http://192.168.68.71:386/switch/close'
    }
    if (device.getBattery() < 20 && !device.isCharging()) {
        url = 'http://192.168.68.71:386/switch/open'
    }
    if (url) {
        let r = http.get(url);
        let msg = r.body.string()
        console.log(`${url}: ${msg}`)
    }

}
function main() {
    updateFile()
}

main()
toast('结束')