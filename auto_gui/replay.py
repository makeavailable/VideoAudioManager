# coding=utf-8
#-------------------------------------------------------------------------------
# Name:        模块1
# Purpose:
#
# Author:      mu
#
# Created:     07/12/2020
# Copyright:   (c) mu 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import time
import os

import pyautogui
import pywinauto
import util
import threading
import cv2
import numpy as np

##app = util.get_Application()
path = util.Conf.path
time_format = util.Conf.time_format

of=None
stoped = False
hook = None

def event_handler(event):
##    print(f'{event.current_key} {event.event_type} {event.pressed_key}')
    if event.current_key  == 'Escape' and event.event_type == 'key down':
        global stoped
        stoped = True
        hook.stop()

def thread_main(hook):
    hook.hook()

def main():
    global hook
    win = app.top_window()
    if not win.exists():
        return
    hook = util.get_hooks(event_handler)
    threading.Thread(target=thread_main, args=(hook,)).start()
    global of
    of = open(os.path.join(path, 'event_sequence.txt'), 'r')
    s = ''
    last_t = 0
    for s in of:
        win.set_focus()
        if stoped:
            break
        s = s.strip()
        print(s, end=' ')
        lst = s.split()
        name = lst[0]
        event = lst[1]
        point = eval(lst[2])
        p = os.path.join(path, '%s-%s-point.png'%(name, event))
        if not os.path.exists(p):
            print(' not exist')
            continue
        full_pic = os.path.join(path, '%s-%s.png'%(name, event))
        util.set_win_size(win, full_pic)
        confidence = 0.9
        rect = win.client_rect()
        l,t,w,h = rect.left,rect.top,rect.width(),rect.height()
        pos = pyautogui.locateOnScreen(p,confidence=confidence, region = (l,t,w,h))
        print(pos, end=' ')
        if not pos:
            pos = point
##            while confidence >= 0.5:
##                confidence -= 0.1
##                pos = pyautogui.locateOnScreen(p,confidence=confidence)
##                if pos:
##                    break
        if not pos:
            util.show_image(full_pic)
            break
        if len(pos) == 2:
            pt = pos
        else:
            pt = pyautogui.center(pos)
        region = util.get_region(win, pt[0], pt[1])
        im_point = pyautogui.screenshot(region=region)
        util.show_image(im_point, p)
        print(pt)
        pyautogui.click(pt)
        last_t = sleep(name, last_t)
    of.close()
    hook.stop()

def sleep(time_str, last_t):
    t = time.mktime(time.strptime(time_str, time_format))
    if not last_t:
        time.sleep(10)
        return t
    sub = t - last_t
    if sub < 1.0:
        sub = 2
    time.sleep(sub)
    return t

def resizeImg(image, height=900):
    h, w = image.shape[:2]
    pro = height / h
    size = (int(w * pro), int(height))
    img = cv2.resize(image, size)
    return img, pro

def findMaxContour(image, point):
    # 寻找边缘
    a = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    contours, _ = a
    print('contours:', len(contours), point)
    # 计算面积
    max_area = 0.0
    max_contour = []
    contour_lst = []
    b = 75
    abs_dist = 1000
    for contour in contours:
        currentArea = cv2.contourArea(contour)
        if currentArea > max_area:
            max_area = currentArea
            max_contour = contour
        dist = cv2.pointPolygonTest(contour, point, True)
        dist = abs(dist)
        if dist < abs_dist:
            abs_dist = dist
            contour_lst.append(contour)
            cv2.drawContours(image, contour, -1, (b, 0, 222), 1)
            b += 75
    print('contour:', len(contour_lst))
    return max_contour, max_area, contour_lst[-1]

# 边缘检测
def getCanny(image):
    # 高斯模糊
    binary = cv2.GaussianBlur(image, (3, 3), 2, 2)
    # 边缘检测
    #binary = cv2.Canny(binary, 60, 240, apertureSize=3)
    binary = cv2.Canny(binary, 50, 120, apertureSize=3)
    # 膨胀操作，尽量使边缘闭合
    kernel = np.ones((3, 3), np.uint8)
    binary = cv2.dilate(binary, kernel, iterations=1)
    return binary

def edge_detect():
    path = r'E:\develop\test\pic2\2021-11-05-22-28-36-LButton(101,425).png'
    outpath = path + '_out.png'
    img = cv2.imread(path)
    img,pro = resizeImg(img)
    print('shape =', img.shape)
    binary_img = getCanny(img)
    cv2.imwrite(outpath.replace('_out', '_out_canny'), binary_img)
    point = (101,425)
    point = (point[0]*pro, point[1]*pro)
    max_contour, max_area, p_contoure = findMaxContour(binary_img, point)
    cv2.imshow('imshow',binary_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    #cv2.drawContours(img, max_contour, -1, (0, 0, 255), 3)
    if len(p_contoure):
        x,y,w,h = cv2.boundingRect(p_contoure)
        img2 = img[y:y+h, x:x+w]
        cv2.imwrite(outpath.replace('_out', '_out2'), img2)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
    cv2.imwrite(outpath, img)

def test_ocr():
    import requests

def test_jdc():
    import requests
    url = 'http://192.168.68.1/jdcapi'
    data = '{"jsonrpc":"2.0","id":1,"method":"call","params":["00000000000000000000000000000000","session","login",{"username":"root","password":"00110566","timeout":600}]}:'
    rsp = requests.post(url, data)
    data = rsp.json()
    ssid = data.get('result')[1].get('ubus_rpc_session')
    data = {
      "jsonrpc": "2.0",
      "id": 1,
      "method": "call",
      "params": [
        ssid,
        "jdcapi.static",
        "web_get_device_list",
        {}
      ]
    }
    jar = requests.cookies.RequestsCookieJar()
    cookies = f'HostAddrIP=192.168.68.1; platform=Atheros; work_mode_flag=0; sessionid={ssid}'
    for cookie in cookies.split(";"):
        key, value = cookie.split("=", 1)
        jar.set(key, value)
    headers = {
        'Content-Type': 'application/json'
    }
    rsp = requests.post(url, json = data, cookies=jar, headers=headers)
    print(rsp.text)

    # disable
    data = {
      "jsonrpc": "2.0",
      "id": 20,
      "method": "call",
      "params": [
        ssid,
        "jdcapi.static",
        "set_macfilter",
        {
          "macpolicy": "deny",
          "macaddr": "70:3A:51:06:AA:55",
          "name": "RedmiNote5-amituofod",
          "mod": "1", # 禁用
          "enable": "1"
        }
      ]
    }
    data = {
      "jsonrpc": "2.0",
      "id": 1,
      "method": "call",
      "params": [
        ssid,
        "jdcapi.static",
        "web_set_device_limit_speed",
        {
          "uid": "703A5106AA55",
          "enable": "0",
          "upload": "0",
          "download": "0"
        }
      ]
    }
    data = {
      "jsonrpc": "2.0",
      "id": 20,
      "method": "call",
      "params": [
        ssid,
        "jdcapi.static",
        "set_macfilter",
        {
          "macpolicy": "deny",
          "macaddr": "70:3A:51:06:AA:55",
          "name": "RedmiNote5-amituofod",
          "mod": "0", # 启用
          "enable": "1"
        }
      ]
    }
    data = {
      "jsonrpc": "2.0",
      "id": 1,
      "method": "call",
      "params": [
        ssid,
        "jdcapi.static",
        "web_set_device_limit_speed",
        {
          "uid": "703A5106AA55", # mac
          "enable": "0",
          "upload": "0",
          "download": "0"
        }
      ]
    }

def ocr_space_file(filename, overlay=False, api_key='helloworld', language='chs'):
    """ OCR.space API request with local file.
        Python3.5 - not tested on 2.7
    :param filename: Your file path & name.
    :param overlay: Is OCR.space overlay required in your response.
                    Defaults to False.
    :param api_key: OCR.space API key.
                    Defaults to 'helloworld'.
    :param language: Language code to be used in OCR.
                    List of available language codes can be found on https://ocr.space/OCRAPI
                    Defaults to 'en'.
    :return: Result in JSON format.
    """

    import requests
    payload = {'isOverlayRequired': overlay,
               'apikey': api_key,
               'language': language,
               }
    with open(filename, 'rb') as f:
        r = requests.post('https://api.ocr.space/parse/image',
                          files={filename: f},
                          data=payload,
                          )
    print(r.text)
    return r.content.decode()

#1.18.3
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common import by
from webdriver.webelement import By
from appium.webdriver.common.touch_action import TouchAction
import time

class AppiumWrapper:
    def __init__(self, driver):
        self.driver = driver
    def __getattr__(self,attr):
        getattr(self.driver, attr)

def 领京豆(driver):
    driver.current_activity == '.WebActivity'
    e=driver.find_element_by_xpath("//*[@text='领京豆']")
    e.click()
    time.sleep(8)
    e = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View[3]/android.view.View[2]/android.view.View/android.widget.Image')
    #e = driver.find_element_by_xpath("//*[@resource-id='hello']")
    e.click()
    time.sleep(3)

    #任务列表
    lst = driver.find_elements_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[3]/android.view.View/android.view.View/*')
    for i,e in enumerate(lst):
        if i == 0: #做任务再升一级可额外得
            continue
        subs = e.find_elements(by.By.XPATH, './android.view.View/android.view.View/android.view.View/android.view.View')
        print('len=', len(subs))
        for view in subs:
            if not view.text:
                print(','.join(get_child_text(view)))
            else:
                print(view.text)
    #driver.back()
    #底部任务列表，如抽京豆
    e = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[4]/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View')
    lst = e.find_elements(by.By.XPATH, './android.view.View/android.view.View')
    task_name = driver.find_element_by_xpath("//*[@resource-id='com.jingdong.app.mall:id/fd']").text
    for e in lst:
        e.click()
        time.sleep(2)
        print(get_title(driver), driver.current_activity)
        while get_title(driver) != task_name:
            driver.back()
            time.sleep(2)

    e = driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[4]/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View")
    e.click()
    time.sleep(2)
    e= driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]")
    e.click()
    driver.back()

def get_title(driver):
    try:
        return driver.find_element_by_xpath("//*[@resource-id='com.jingdong.app.mall:id/fd']").text
    except:
        pass
    try:
        return driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView").text
    except:
        pass

def 东东工厂(driver):
    pass

def test_appium():
    # 定义启动设备需要的参数
    desired_caps = {
      "platformName": "Android",
      "platformVersion": "7.1.2",
      "deviceName": "127.0.0.1:21533",
      "appPackage": "com.jingdong.app.mall",
      "appActivity": "com.jingdong.app.mall.main.MainActivity",
      "noReset": True,
      "newCommandTimeout":10000
    }
    # 设备系统
##    desired_caps['platformName'] = 'Android'
##    # 设备系统版本号
##    desired_caps['platformVersion'] = '7.1.2'
##    # 设备名称
##    desired_caps['deviceName'] = '127.0.0.1:21533'
##    ## 要测试的应用的地址
##    #desired_caps['app'] = 'xxx.apk'   # 注意是 apk 文件在电脑上的绝对路径
##    # 应用的包名
##    desired_caps['appPackage'] = 'com.jingdong.app.mall'
##    desired_caps['appActivity'] = 'com.jingdong.app.mall.main.MainActivity'
##    desired_caps['noReset'] = True

    driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
    globals()['driver']=driver
    try:
        e=driver.find_element_by_xpath("//*[@text='立即登录']")
        if e:
            print('需要登陆')
            e.click()
    except:
        pass

    东东工厂(driver)

    京喜 = {
      "platformName": "Android",
      "platformVersion": "7.1.2",
      "deviceName": "127.0.0.1:21533",
      "appPackage": "com.jd.pingou",
      "appActivity": "com.jd.pingou.home.HomeActivity",
      "noReset": False,
      "newCommandTimeout":10000
    }
    driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', 京喜)
    driver.find_element_by_xpath("//*[@text='京喜牧场']").click()

def get_child_text(element):
    lst = element.find_elements(by.By.XPATH, '//*[@text]')
    texts = []
    for e in lst:
        if e.text:
            texts.append(e.text)
    return texts

if __name__ == '__main__':
##    test_jdc()
##    ocr_space_file(r'E:\develop\test\pic2\2021-11-05-22-28-36-LButton(101,425).png', True)
    test_appium()
