# The lines up to and including sys.stderr should always come first
# Then any errors that occur later get reported to the console
# If you'd prefer to report errors to a file, you can do that instead here.
import re
def regxFind():
    reg=globals().get('lastRegx','')
    a=u'\u8bf7\u8f93\u5165\u6b63\u5219\u8868\u8fbe\u5f0f'.encode('gb2312')
    reg = notepad.prompt(a,a,reg)
    assert reg
    globals()['lastRegx']=reg
    console.write(reg+'\n')
    editor.research(reg, matchFound)

def matchFound(m):
    linenumber = editor.lineFromPosition(m.start())+1
    path = notepad.getCurrentFilename()
    txt = '  File "%s", line %d\n'%(path,linenumber)
    ce=Cell(txt,[2])
    console.editor.setReadOnly(0)
    console.editor.addStyledText(ce)
    console.write(m.group()+'\n')

if __name__ == "__main__":
    regxFind()
